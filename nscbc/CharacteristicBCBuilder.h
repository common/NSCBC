/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   CharacteristicBCBuilder.h
 */

#ifndef CHARACTERISTICBCBUILDER_H_
#define CHARACTERISTICBCBUILDER_H_

#include <spatialops/structured/stencil/OneSidedOperatorTypes.h>

#include "TagManager.h"
#include "NSCBCToolsAndDefs.h"

#include <expression/MaskedExpression.h> //for setting Lk's to zero

//--- Modifier Includes ---//
#include "modifierexpressions/DensityNSCBCModifier.h"
#include "modifierexpressions/EnergyNSCBCModifier.h"
#include "modifierexpressions/MomentumNSCBCModifier.h"
#include "modifierexpressions/MomentumOffDiagonalModifier.h"
#include "modifierexpressions/SpeciesNSCBCModifier.h"
#include "modifierexpressions/DensityHardInflowModifier.h"

//--- L Includes ---//
//Calculated
#include "Lexpressions/L1L2.h"
#include "Lexpressions/L3L4L5Diagonal.h"
#include "Lexpressions/L3L4L5OffDiagonal.h"
#include "Lexpressions/Lk.h"
//Set
#include "Lexpressions/L1Set.h"
#include "Lexpressions/L345Set.h"
#include "Lexpressions/L345OffDiagSet.h"
#include "Lexpressions/LkSet.h"
//Inflow
#include "Lexpressions/L1L2HardInflow.h"

//--- R Includes ---//
#include "Lexpressions/RPressure.h"

namespace Expr{
  class ExpressionFactory;
  class Tag;
}

namespace NSCBC{

  /**
    \enum NonreflectingSubSwitch
    \author Mike Hansen
  
    \brief Makes a more expressive API for turning on/off wave subtraction in nonreflecting BCs
  */
  enum class NonreflectingSubSwitch
  {
    SUBTRACTION_ON,
    SUBTRACTION_OFF
  };

  /**
    \enum CanOverwriteExpr
    \author Mike Hansen
  
    \brief Makes a more expressive API for turning on/off expression overwriting
  */
  enum class CanOverwriteExpr
  {
    OVERWRITE_ON,
    OVERWRITE_OFF
  };



  /**
   *  \class BCBuilder
   *  \author Author: Derek Cline
   *
   *  \brief Constructs and attaches every expression associated with the NSCBC
   *
   *  \param mySpec Struct containing various NSCBC info (side, type, etc).
   *  \param specMW vector of doubles that contain the values of the molecular
   *                weights of transported species
   *  \param gasConstant Universal gas constant
   *  \param tags Tag manager of tags of each field required for applying NSCBC
   *  \param doPerp1 boolean for when we are in a 1D or 2D case.  For an x face,
   *                 doPerp1 is true if we have a y direction.  For a y or z face,
   *                 doPerp1 is true if we have an x direction.
   *  \param doPerp2 boolean for when we are in a 1D or 2D case.  For an x or y face,
   *                 doPerp2 is true if we have a z direction.  For a z face,
   *                 doPerp2 is true if we have a y direction.
   *  \param subtractionSwitch scoped enum to turn on/off wave amplitude subtraction for
   *                           nonreflecting boundaries
   *  \param patchID ID associated with a particular patch.  For LBMS, this corresponds
   *                 to the bundle ID.
   *  \param canOverwriteExpressions whether or not expressions can be registered to the
   *                 same factory multiple times by overwriting old instances.
   *  \tparam FieldT Volume field type.  Everything associated with this library
   *                 utilizes volume fields.  In the case of derivatives, we
   *                 use one sided stencils that operate on volume fields to
   *                 produce a volume field.
   *
   *
   * \par Example
   *  \code{.cpp}
   *  new NSCBC::BCBuilder<FieldT>( NSCBCinfo,
   *                                CanteraObjects::molecular_weights(),
   *                                CanteraObjects::gas_constant(),
   *                                tagManager,
   *                                doPerp1,
   *                                doPerp2,
   *                                NSCBC::NonreflectingSubSwitch::SUBTRACTION_OFF,
   *                                bundle->id() )
   *  \endcode
   */
  template< typename FieldT >
  class BCBuilder{

    typedef typename SpatialOps::SpatialMask<FieldT> sptMask;

    NSCBCInfo<FieldT> mySpec_;

    std::set< TransportVal > modifiersBuilt_;
    TagManager tMgr_;

    bool initializedL_;
    const int id_;
    const std::vector<double> specMW_;
    const int nSpecies_;
    const double gasConstant_;
    const bool doPerp1_;
    const bool doPerp2_;
    const bool doNonreflectingSubtraction_;
    const bool canOverwriteExpr_;
    const BCType boundaryType_;
    const SpatialOps::BCSide side_;
    const sptMask bcMask_;
    const Direction dir_;
    const std::string jobName_;

    void create_l_tags();
    void register_l_calc( Expr::ExpressionFactory& execFac );
    void register_l_set( Expr::ExpressionFactory& execFac );

  public:

    BCBuilder( const NSCBCInfo<FieldT>& mySpec,
               const std::vector<double> specMW,
               const double gasConstant,
               const TagManager& tags,
               const bool doPerp1,
               const bool doPerp2,
               const NonreflectingSubSwitch subtractionSwitch = NonreflectingSubSwitch::SUBTRACTION_OFF,
               const int patchID = 0,
               const CanOverwriteExpr canOverwriteExpressions = CanOverwriteExpr::OVERWRITE_OFF )
      : mySpec_( mySpec ),
        tMgr_( tags ),
        initializedL_( false ),
        id_( patchID ),
        specMW_( specMW ),
        nSpecies_( specMW.size() ),
        gasConstant_( gasConstant ),
        doPerp1_( doPerp1 ),
        doPerp2_( doPerp2 ),
        doNonreflectingSubtraction_( subtractionSwitch == NonreflectingSubSwitch::SUBTRACTION_ON ),
        canOverwriteExpr_( canOverwriteExpressions == CanOverwriteExpr::OVERWRITE_ON ),
        boundaryType_( mySpec_.type ),
        side_( mySpec_.side ),
        bcMask_( mySpec_.mask),
        dir_( mySpec_.direction ),
        jobName_( mySpec_.jobName )
      {}

    ~BCBuilder(){};
    void set_tag_manager( const TagManager& tags ){ tMgr_ = tags; }
    void attach_rhs_modifier( Expr::ExpressionFactory& execFac,
                              const Expr::Tag& rhsTag,
                              const TransportVal quantity,
                              const int specNumber );

    inline SpatialOps::BCSide get_boundary_side() const{ return side_; }
    inline BCType get_boundary_type() const{ return boundaryType_; }
    inline Direction get_boundary_direction() const{ return dir_; }
    inline std::string get_job_name() const{ return jobName_; }

    inline sptMask get_mask() const{ return bcMask_; }

    inline std::string face() const{ return NSCBC::face_string(dir_); }
    inline std::string side() const{ return NSCBC::side_string(side_); }
    inline std::string type() const{ return NSCBC::type_string(boundaryType_); }
  };


  template< typename FieldT >
  void
  BCBuilder<FieldT>::attach_rhs_modifier( Expr::ExpressionFactory& execFac,
                                          const Expr::Tag& rhsTag,
                                          const TransportVal quantity,
                                          const int specNumber )
  {
    if( !initializedL_ ){
      register_l_calc( execFac );
      register_l_set(  execFac );
      initializedL_ = true;
    }
    double side = 0.0;
    const double add =  1.0;
    const double sub = -1.0;
    switch( side_ ){
      case SpatialOps::PLUS_SIDE:
        side = 1.0;
        break;
      case SpatialOps::MINUS_SIDE:
        side = -1.0;
        break;
      default:
        std::ostringstream msg;
        msg << __FILE__ << " : " << __LINE__ << std::endl
            << "No side was given" << std::endl;
        throw std::runtime_error( msg.str() );
    }
    switch( boundaryType_ ){
      case WALL:
        break;
      case HARD_INFLOW:
        if( quantity == DENSITY ){
          typedef typename DensityHardInflowModifier<FieldT>::Builder RhoModifier;
          execFac.register_expression( new RhoModifier( tMgr_[DF_DENSITY_ADD], bcMask_, tMgr_[L1_M], tMgr_[L2_M], tMgr_[RHO], tMgr_[P], tMgr_[RP], tMgr_[MMW], tMgr_[CV], tMgr_[C] ), canOverwriteExpr_, id_ );
          execFac.attach_modifier_expression( tMgr_[DF_DENSITY_ADD], rhsTag, -999999, canOverwriteExpr_);
        }
        break;

      case NONREFLECTING:
        switch( quantity ){
          case DENSITY:
            typedef typename DensityNSCBCModifier<FieldT>::Builder RhoModifier;
            execFac.register_expression( new RhoModifier( tMgr_[DF_DENSITY_ADD], bcMask_, side, tMgr_[C], tMgr_[L1_M], tMgr_[L2_M], tMgr_[L345D_M], add ), canOverwriteExpr_, id_ );
            execFac.attach_modifier_expression( tMgr_[DF_DENSITY_ADD], rhsTag, -999999, canOverwriteExpr_);

            execFac.register_expression( new RhoModifier( tMgr_[DF_DENSITY_SUB], bcMask_, side, tMgr_[C], tMgr_[L1], tMgr_[L2], tMgr_[L345D], sub ), canOverwriteExpr_, id_ );
            if( doNonreflectingSubtraction_ ){
              execFac.attach_modifier_expression( tMgr_[DF_DENSITY_SUB], rhsTag, -999999, canOverwriteExpr_);
            }

            modifiersBuilt_.insert( DENSITY );
            break;
          case MOMENTUM_X:
            typedef typename MomentumNSCBCModifier<FieldT>::Builder       MomentumModifier;
            typedef typename MomentumOffDiagonalModifier<FieldT>::Builder OffDiagonalModifier;

            switch( dir_ ){
              case XDIR:
                execFac.register_expression( new MomentumModifier   ( tMgr_[DF_MOMENTUM_X_ADD], bcMask_, side,       tMgr_[RHO], tMgr_[C], tMgr_[U], tMgr_[L1_M], tMgr_[L2_M], tMgr_[L345D_M], add  ), canOverwriteExpr_, id_ );
                execFac.register_expression( new MomentumModifier   ( tMgr_[DF_MOMENTUM_X_SUB], bcMask_, side,       tMgr_[RHO], tMgr_[C], tMgr_[U], tMgr_[L1], tMgr_[L2], tMgr_[L345D], sub  ), canOverwriteExpr_, id_ );
                break;
              case YDIR:
                assert( doPerp1_ );
                execFac.register_expression( new OffDiagonalModifier( tMgr_[DF_MOMENTUM_X_ADD], bcMask_, side, 1.0,  tMgr_[RHO], tMgr_[C], tMgr_[U], tMgr_[L1_M], tMgr_[L2_M], tMgr_[L345D_M], tMgr_[L345O2_M], add ), canOverwriteExpr_, id_ );
                execFac.register_expression( new OffDiagonalModifier( tMgr_[DF_MOMENTUM_X_SUB], bcMask_, side, 1.0,  tMgr_[RHO], tMgr_[C], tMgr_[U], tMgr_[L1], tMgr_[L2], tMgr_[L345D], tMgr_[L345O2], sub ), canOverwriteExpr_, id_ );
                break;
              case ZDIR:
                assert( doPerp1_ );
                execFac.register_expression( new OffDiagonalModifier( tMgr_[DF_MOMENTUM_X_ADD], bcMask_, side, -1.0, tMgr_[RHO], tMgr_[C], tMgr_[U], tMgr_[L1_M], tMgr_[L2_M], tMgr_[L345D_M], tMgr_[L345O2_M], add ), canOverwriteExpr_, id_ );                
                execFac.register_expression( new OffDiagonalModifier( tMgr_[DF_MOMENTUM_X_SUB], bcMask_, side, -1.0, tMgr_[RHO], tMgr_[C], tMgr_[U], tMgr_[L1], tMgr_[L2], tMgr_[L345D], tMgr_[L345O2], sub ), canOverwriteExpr_, id_ );            
                break;
              case NODIR:
                std::ostringstream msg;
                msg << __FILE__ << " : " << __LINE__ << std::endl
                    << "No direction in the X Momentum BCBuilder attach modifier" << std::endl;
                throw std::runtime_error( msg.str() );
            }
            execFac.attach_modifier_expression( tMgr_[DF_MOMENTUM_X_ADD], rhsTag, -999999, canOverwriteExpr_);
            if( doNonreflectingSubtraction_ ){
              execFac.attach_modifier_expression( tMgr_[DF_MOMENTUM_X_SUB], rhsTag, -999999, canOverwriteExpr_);
            }

            modifiersBuilt_.insert( MOMENTUM_X );
            break;
              case MOMENTUM_Y:
                typedef typename MomentumNSCBCModifier<FieldT>::Builder       MomentumModifier;
                typedef typename MomentumOffDiagonalModifier<FieldT>::Builder OffDiagonalModifier;

                switch( dir_ ){
                  case XDIR:
                    assert( doPerp1_ );
                    execFac.register_expression( new OffDiagonalModifier( tMgr_[DF_MOMENTUM_Y_ADD], bcMask_, side, -1.0, tMgr_[RHO], tMgr_[C], tMgr_[V], tMgr_[L1_M], tMgr_[L2_M], tMgr_[L345D_M], tMgr_[L345O2_M], add ), canOverwriteExpr_, id_ );               
                    execFac.register_expression( new OffDiagonalModifier( tMgr_[DF_MOMENTUM_Y_SUB], bcMask_, side, -1.0, tMgr_[RHO], tMgr_[C], tMgr_[V], tMgr_[L1], tMgr_[L2], tMgr_[L345D], tMgr_[L345O2], sub ), canOverwriteExpr_, id_ );
                    break;
                  case YDIR:
                    execFac.register_expression( new MomentumModifier   ( tMgr_[DF_MOMENTUM_Y_ADD], bcMask_, side,       tMgr_[RHO], tMgr_[C], tMgr_[V], tMgr_[L1_M], tMgr_[L2_M], tMgr_[L345D_M], add ), canOverwriteExpr_, id_ );
                    execFac.register_expression( new MomentumModifier   ( tMgr_[DF_MOMENTUM_Y_SUB], bcMask_, side,       tMgr_[RHO], tMgr_[C], tMgr_[V], tMgr_[L1], tMgr_[L2], tMgr_[L345D], sub ), canOverwriteExpr_, id_ );
                    break;
                  case ZDIR:
                    assert( doPerp2_ );
                    execFac.register_expression( new OffDiagonalModifier( tMgr_[DF_MOMENTUM_Y_ADD], bcMask_, side, 1.0,  tMgr_[RHO], tMgr_[C], tMgr_[V], tMgr_[L1_M], tMgr_[L2_M], tMgr_[L345D_M], tMgr_[L345O1_M], add ), canOverwriteExpr_, id_ );
                    execFac.register_expression( new OffDiagonalModifier( tMgr_[DF_MOMENTUM_Y_SUB], bcMask_, side, 1.0,  tMgr_[RHO], tMgr_[C], tMgr_[V], tMgr_[L1], tMgr_[L2], tMgr_[L345D], tMgr_[L345O1], sub ), canOverwriteExpr_, id_ );
                    break;
                  case NODIR:
                    std::ostringstream msg;
                    msg << __FILE__ << " : " << __LINE__ << std::endl
                        << "No direction in the Y Momentum BCBuilder attach modifier" << std::endl;
                    throw std::runtime_error( msg.str() );
                }
                execFac.attach_modifier_expression( tMgr_[DF_MOMENTUM_Y_ADD], rhsTag, -999999, canOverwriteExpr_);
                if( doNonreflectingSubtraction_ ){
                  execFac.attach_modifier_expression( tMgr_[DF_MOMENTUM_Y_SUB], rhsTag, -999999, canOverwriteExpr_);
                }

                modifiersBuilt_.insert( MOMENTUM_Y );
                break;
                  case MOMENTUM_Z:
                    typedef typename MomentumNSCBCModifier<FieldT>::Builder MomentumModifier;
                    typedef typename MomentumOffDiagonalModifier<FieldT>::Builder OffDiagonalModifier;

                    switch( dir_ ){
                      case XDIR:
                        assert( doPerp2_ );
                        execFac.register_expression( new OffDiagonalModifier( tMgr_[DF_MOMENTUM_Z_ADD], bcMask_, side, 1.0, tMgr_[RHO], tMgr_[C], tMgr_[W], tMgr_[L1_M], tMgr_[L2_M], tMgr_[L345D_M], tMgr_[L345O1_M], add ), canOverwriteExpr_, id_ );
                        execFac.register_expression( new OffDiagonalModifier( tMgr_[DF_MOMENTUM_Z_SUB], bcMask_, side, 1.0, tMgr_[RHO], tMgr_[C], tMgr_[W], tMgr_[L1], tMgr_[L2], tMgr_[L345D], tMgr_[L345O1], sub ), canOverwriteExpr_, id_ );
                        break;
                      case YDIR:
                        assert( doPerp2_ );
                        execFac.register_expression( new OffDiagonalModifier( tMgr_[DF_MOMENTUM_Z_ADD], bcMask_, side, -1.0, tMgr_[RHO], tMgr_[C], tMgr_[W], tMgr_[L1_M], tMgr_[L2_M], tMgr_[L345D_M], tMgr_[L345O1_M], add ), canOverwriteExpr_, id_ );
                        execFac.register_expression( new OffDiagonalModifier( tMgr_[DF_MOMENTUM_Z_SUB], bcMask_, side, -1.0, tMgr_[RHO], tMgr_[C], tMgr_[W], tMgr_[L1], tMgr_[L2], tMgr_[L345D], tMgr_[L345O1], sub ), canOverwriteExpr_, id_ );
                        break;
                      case ZDIR:
                        execFac.register_expression( new MomentumModifier   ( tMgr_[DF_MOMENTUM_Z_ADD], bcMask_, side,       tMgr_[RHO], tMgr_[C], tMgr_[W], tMgr_[L1_M], tMgr_[L2_M], tMgr_[L345D_M], add ), canOverwriteExpr_, id_ );
                        execFac.register_expression( new MomentumModifier   ( tMgr_[DF_MOMENTUM_Z_SUB], bcMask_, side,       tMgr_[RHO], tMgr_[C], tMgr_[W], tMgr_[L1], tMgr_[L2], tMgr_[L345D], sub ), canOverwriteExpr_, id_ );
                        break;
                      case NODIR:
                        std::ostringstream msg;
                        msg << __FILE__ << " : " << __LINE__ << std::endl
                            << "No direction in the Z Momentum BCBuilder attach modifier" << std::endl;
                        throw std::runtime_error( msg.str() );
                    }
                    execFac.attach_modifier_expression( tMgr_[DF_MOMENTUM_Z_ADD], rhsTag, -999999, canOverwriteExpr_);
                    if( doNonreflectingSubtraction_ ){
                      execFac.attach_modifier_expression( tMgr_[DF_MOMENTUM_Z_SUB], rhsTag, -999999, canOverwriteExpr_);
                    }

                    modifiersBuilt_.insert( MOMENTUM_Z );
                    break;
                      case ENERGY: {
                        typedef typename EnergyNSCBCModifier<FieldT>::Builder RhoE0Modifier;
                        Expr::Tag parVel; Expr::Tag perp1Vel; Expr::Tag perp2Vel;
                        switch( dir_ ){
                          case XDIR: parVel = tMgr_[U]; perp1Vel = tMgr_[V]; perp2Vel = tMgr_[W]; break;
                          case YDIR: parVel = tMgr_[V]; perp1Vel = tMgr_[U]; perp2Vel = tMgr_[W]; break;
                          case ZDIR: parVel = tMgr_[W]; perp1Vel = tMgr_[U]; perp2Vel = tMgr_[V]; break;
                          case NODIR:
                            std::ostringstream msg;
                            msg << __FILE__ << " : " << __LINE__ << std::endl
                                << "No direction in the Energy BCBuilder attach modifier" << std::endl;
                            throw std::runtime_error( msg.str() );
                        }

                        if( doPerp1_ && doPerp2_){
                          execFac.register_expression( new RhoE0Modifier( tMgr_[DF_ENERGY_ADD], mySpec_, side, tMgr_[E0], tMgr_[C], tMgr_[RHO], parVel, perp1Vel, perp2Vel, tMgr_[T], tMgr_[CP], tMgr_[CV], tMgr_[MMW],
                                                                          tMgr_[H_N], tMgr_[L1_M], tMgr_[L2_M], tMgr_[L345D_M], tMgr_[L345O1_M], tMgr_[L345O2_M], tMgr_[LKS_M],
                                                                          specMW_, gasConstant_, tMgr_.doSpecies_, add ),
                                                       canOverwriteExpr_, id_ );
                          execFac.register_expression( new RhoE0Modifier( tMgr_[DF_ENERGY_SUB], mySpec_, side, tMgr_[E0], tMgr_[C], tMgr_[RHO], parVel, perp1Vel, perp2Vel, tMgr_[T], tMgr_[CP], tMgr_[CV], tMgr_[MMW],
                                                                          tMgr_[H_N], tMgr_[L1], tMgr_[L2], tMgr_[L345D], tMgr_[L345O1], tMgr_[L345O2], tMgr_[LKS],
                                                                          specMW_, gasConstant_, tMgr_.doSpecies_, sub ),
                                                       canOverwriteExpr_, id_ );
                        }
                        else if( !doPerp1_ && doPerp2_ ){
                          execFac.register_expression( new RhoE0Modifier( tMgr_[DF_ENERGY_ADD], mySpec_, side, tMgr_[E0], tMgr_[C], tMgr_[RHO], parVel, perp2Vel, tMgr_[T], tMgr_[CP], tMgr_[CV], tMgr_[MMW],
                                                                          tMgr_[H_N], tMgr_[L1_M], tMgr_[L2_M], tMgr_[L345D_M], tMgr_[L345O1_M], tMgr_[LKS_M],
                                                                          specMW_, gasConstant_, tMgr_.doSpecies_, add ),
                                                       canOverwriteExpr_, id_ );
                          execFac.register_expression( new RhoE0Modifier( tMgr_[DF_ENERGY_SUB], mySpec_, side, tMgr_[E0], tMgr_[C], tMgr_[RHO], parVel, perp2Vel, tMgr_[T], tMgr_[CP], tMgr_[CV], tMgr_[MMW],
                                                                          tMgr_[H_N], tMgr_[L1], tMgr_[L2], tMgr_[L345D], tMgr_[L345O1], tMgr_[LKS],
                                                                          specMW_, gasConstant_, tMgr_.doSpecies_, sub ),
                                                       canOverwriteExpr_, id_ );
                        }
                        else if( !doPerp2_ && doPerp1_ ){
                          execFac.register_expression( new RhoE0Modifier( tMgr_[DF_ENERGY_ADD], mySpec_, side, tMgr_[E0], tMgr_[C], tMgr_[RHO], parVel, perp1Vel, tMgr_[T], tMgr_[CP], tMgr_[CV], tMgr_[MMW],
                                                                          tMgr_[H_N], tMgr_[L1_M], tMgr_[L2_M], tMgr_[L345D_M], tMgr_[L345O2_M], tMgr_[LKS_M],
                                                                          specMW_, gasConstant_, tMgr_.doSpecies_, add ),
                                                       canOverwriteExpr_, id_ );
                          execFac.register_expression( new RhoE0Modifier( tMgr_[DF_ENERGY_SUB], mySpec_, side, tMgr_[E0], tMgr_[C], tMgr_[RHO], parVel, perp1Vel, tMgr_[T], tMgr_[CP], tMgr_[CV], tMgr_[MMW],
                                                                          tMgr_[H_N], tMgr_[L1], tMgr_[L2], tMgr_[L345D], tMgr_[L345O2], tMgr_[LKS],
                                                                          specMW_, gasConstant_, tMgr_.doSpecies_, sub ),
                                                       canOverwriteExpr_, id_ );
                        }
                        else{
                          execFac.register_expression( new RhoE0Modifier( tMgr_[DF_ENERGY_ADD], mySpec_, side, tMgr_[E0], tMgr_[C], tMgr_[RHO], parVel, tMgr_[T], tMgr_[CP], tMgr_[CV], tMgr_[MMW],
                                                                          tMgr_[H_N], tMgr_[L1_M], tMgr_[L2_M], tMgr_[L345D_M], tMgr_[LKS_M],
                                                                          specMW_, gasConstant_, tMgr_.doSpecies_, add ),
                                                       canOverwriteExpr_, id_ );
                          execFac.register_expression( new RhoE0Modifier( tMgr_[DF_ENERGY_SUB], mySpec_, side, tMgr_[E0], tMgr_[C], tMgr_[RHO], parVel, tMgr_[T], tMgr_[CP], tMgr_[CV], tMgr_[MMW],
                                                                          tMgr_[H_N], tMgr_[L1], tMgr_[L2], tMgr_[L345D], tMgr_[LKS],
                                                                          specMW_, gasConstant_, tMgr_.doSpecies_, sub ),
                                                       canOverwriteExpr_, id_ );
                        }
                        execFac.attach_modifier_expression( tMgr_[DF_ENERGY_ADD], rhsTag, -999999, canOverwriteExpr_);
                        if( doNonreflectingSubtraction_ ){
                          execFac.attach_modifier_expression( tMgr_[DF_ENERGY_SUB], rhsTag, -999999, canOverwriteExpr_);
                        }
                        modifiersBuilt_.insert( ENERGY );
                        break;
                      }
                      case SPECIES:
                        assert( tMgr_.doSpecies_ == true);
                        typedef typename SpeciesNSCBCModifier<FieldT>::Builder SpeciesModifier;

                        execFac.register_expression( new SpeciesModifier( tMgr_[DFn_S_ADD][specNumber], bcMask_, side, tMgr_[RHO], tMgr_[C], tMgr_[Y_N][specNumber],
                                                                          tMgr_[L1_M], tMgr_[L2_M], tMgr_[L345D_M], tMgr_[LKS_M][specNumber], add ),
                                                     canOverwriteExpr_, id_ );
                        execFac.register_expression( new SpeciesModifier( tMgr_[DFn_S_SUB][specNumber], bcMask_, side, tMgr_[RHO], tMgr_[C], tMgr_[Y_N][specNumber],
                                                                          tMgr_[L1], tMgr_[L2], tMgr_[L345D], tMgr_[LKS][specNumber], sub ),
                                                     canOverwriteExpr_, id_ );
                        execFac.attach_modifier_expression( tMgr_[DFn_S_ADD][specNumber], rhsTag, -999999, canOverwriteExpr_);
                        if( doNonreflectingSubtraction_ ){
                          execFac.attach_modifier_expression( tMgr_[DFn_S_SUB][specNumber], rhsTag, -999999, canOverwriteExpr_);
                        }
                        modifiersBuilt_.insert( SPECIES );
                        break;
                      default:
                        std::ostringstream msg;
                        msg << __FILE__ << " : " << __LINE__ << std::endl
                            << "No transport equation is assigned in the BCBuilder" << std::endl
                            << "The assigned type is: " << quantity << std::endl
                            << "Candidates are DENSITY, ENERGY, MOMENTUM_X, MOMENTUM_Y, MOMENTUM_Z and SPECIES " << std::endl;
                        throw std::runtime_error( msg.str() );
        } // switch( quantity )

        break;  // case NONREFLECTING

        default:
          std::ostringstream msg;
          msg << __FILE__ << " : " << __LINE__ << std::endl
              << "No NSCBC type is assigned in the BCBuilder, given type is:  "
              << type() << std::endl;
          throw std::runtime_error( msg.str() );
    } // switch( boundaryType_ )
  }

  template< typename FieldT >
  void
  BCBuilder<FieldT>::create_l_tags()
  {
    using Expr::Tag;
    const std::string faceSideType = face_string( dir_ ) + side_string( side_ ) + type_string( boundaryType_ ) + jobName_;
    tMgr_.add_tag( std::make_pair( L1, Tag("L1"+faceSideType, Expr::STATE_NONE) ) );
    tMgr_.add_tag( std::make_pair( L2, Tag("L2"+faceSideType, Expr::STATE_NONE) ) );

    tMgr_.add_tag( std::make_pair( DF_DENSITY_SUB,    Expr::Tag("DFn_DENSITY_SUB"   +faceSideType, Expr::STATE_NONE) ) );
    tMgr_.add_tag( std::make_pair( DF_DENSITY_ADD,    Expr::Tag("DFn_DENSITY_ADD"   +faceSideType, Expr::STATE_NONE) ) );
    tMgr_.add_tag( std::make_pair( DF_MOMENTUM_X_SUB, Expr::Tag("DFn_MOMENTUM_X_SUB"+faceSideType, Expr::STATE_NONE) ) );
    tMgr_.add_tag( std::make_pair( DF_MOMENTUM_X_ADD, Expr::Tag("DFn_MOMENTUM_X_ADD"+faceSideType, Expr::STATE_NONE) ) );
    tMgr_.add_tag( std::make_pair( DF_MOMENTUM_Y_SUB, Expr::Tag("DFn_MOMENTUM_Y_SUB"+faceSideType, Expr::STATE_NONE) ) );
    tMgr_.add_tag( std::make_pair( DF_MOMENTUM_Y_ADD, Expr::Tag("DFn_MOMENTUM_Y_ADD"+faceSideType, Expr::STATE_NONE) ) );
    tMgr_.add_tag( std::make_pair( DF_MOMENTUM_Z_SUB, Expr::Tag("DFn_MOMENTUM_Z_SUB"+faceSideType, Expr::STATE_NONE) ) );
    tMgr_.add_tag( std::make_pair( DF_MOMENTUM_Z_ADD, Expr::Tag("DFn_MOMENTUM_Z_ADD"+faceSideType, Expr::STATE_NONE) ) );
    tMgr_.add_tag( std::make_pair( DF_ENERGY_SUB,     Expr::Tag("DFn_ENERGY_SUB"    +faceSideType, Expr::STATE_NONE) ) );
    tMgr_.add_tag( std::make_pair( DF_ENERGY_ADD,     Expr::Tag("DFn_ENERGY_ADD"    +faceSideType, Expr::STATE_NONE) ) );

    tMgr_.add_tag( std::make_pair( RP, Expr::Tag("RP"+faceSideType, Expr::STATE_NONE) ) );

    tMgr_.add_tag( std::make_pair( L1_M, Expr::Tag("NR_L1"+faceSideType, Expr::STATE_NONE) ) );
    tMgr_.add_tag( std::make_pair( L2_M, Expr::Tag("L2"   +faceSideType, Expr::STATE_NONE) ) );

    switch( dir_ ){
      case XDIR:
        tMgr_.add_tag( std::make_pair( L345D,    Tag("L3"+faceSideType,    Expr::STATE_NONE) ) );
        tMgr_.add_tag( std::make_pair( L345O1,   Tag("L4"+faceSideType,    Expr::STATE_NONE) ) );
        tMgr_.add_tag( std::make_pair( L345O2,   Tag("L5"+faceSideType,    Expr::STATE_NONE) ) );
        tMgr_.add_tag( std::make_pair( L345D_M,  Tag("NR_L3"+faceSideType, Expr::STATE_NONE) ) );
        tMgr_.add_tag( std::make_pair( L345O1_M, Tag("NR_L4"+faceSideType, Expr::STATE_NONE) ) );
        tMgr_.add_tag( std::make_pair( L345O2_M, Tag("NR_L5"+faceSideType, Expr::STATE_NONE) ) );
        break;
      case YDIR:
        tMgr_.add_tag( std::make_pair( L345D,    Tag("L4"+faceSideType,    Expr::STATE_NONE) ) );
        tMgr_.add_tag( std::make_pair( L345O1,   Tag("L3"+faceSideType,    Expr::STATE_NONE) ) );
        tMgr_.add_tag( std::make_pair( L345O2,   Tag("L5"+faceSideType,    Expr::STATE_NONE) ) );
        tMgr_.add_tag( std::make_pair( L345D_M,  Tag("NR_L4"+faceSideType, Expr::STATE_NONE) ) );
        tMgr_.add_tag( std::make_pair( L345O1_M, Tag("NR_L3"+faceSideType, Expr::STATE_NONE) ) );
        tMgr_.add_tag( std::make_pair( L345O2_M, Tag("NR_L5"+faceSideType, Expr::STATE_NONE) ) );
        break;
      case ZDIR:
        tMgr_.add_tag( std::make_pair( L345D,    Tag("L5"+faceSideType,    Expr::STATE_NONE) ) );
        tMgr_.add_tag( std::make_pair( L345O1,   Tag("L3"+faceSideType,    Expr::STATE_NONE) ) );
        tMgr_.add_tag( std::make_pair( L345O2,   Tag("L4"+faceSideType,    Expr::STATE_NONE) ) );
        tMgr_.add_tag( std::make_pair( L345D_M,  Tag("NR_L5"+faceSideType, Expr::STATE_NONE) ) );
        tMgr_.add_tag( std::make_pair( L345O1_M, Tag("NR_L3"+faceSideType, Expr::STATE_NONE) ) );
        tMgr_.add_tag( std::make_pair( L345O2_M, Tag("NR_L4"+faceSideType, Expr::STATE_NONE) ) );
        break;
      case NODIR:
        std::ostringstream msg;
        msg << __FILE__ << " : " << __LINE__ << std::endl
            << "No direction in the create_tag function" << std::endl;
        throw std::runtime_error( msg.str() );
    }

    if( tMgr_.doSpecies_ ){
      Expr::TagList tagListCalc, tagListSet, tagListAdd, tagListSub;
      for( auto i=0; i<nSpecies_-1; ++i ){
        const std::string s = boost::lexical_cast<std::string>(i);
        tagListCalc.push_back( Tag("LK_"            +s+faceSideType, Expr::STATE_NONE) );
        tagListSet.push_back ( Tag("LK_M_"          +s+faceSideType, Expr::STATE_NONE) );
        tagListSub.push_back ( Tag("DFn_SPECIES_SUB"+s+faceSideType, Expr::STATE_NONE) );
        tagListAdd.push_back ( Tag("DFn_SPECIES_ADD"+s+faceSideType, Expr::STATE_NONE) );
      }
      tMgr_.add_taglist( std::make_pair( LKS,       tagListCalc ) );
      tMgr_.add_taglist( std::make_pair( LKS_M,     tagListSet  ) );
      tMgr_.add_taglist( std::make_pair( DFn_S_SUB, tagListSub  ) );
      tMgr_.add_taglist( std::make_pair( DFn_S_ADD, tagListAdd  ) );
    }
    else{
      Expr::TagList tags;
      tags.push_back( Tag( "", Expr::INVALID_CONTEXT ) );
      tMgr_.add_taglist( std::make_pair( LKS,   tags ) );
      tMgr_.add_taglist( std::make_pair( LKS_M, tags ) );
    }
  }

  template< typename FieldT >
  void
  BCBuilder<FieldT>::register_l_calc( Expr::ExpressionFactory& execFac )
  {
    using SpatialOps::PLUS_SIDE;
    using SpatialOps::MINUS_SIDE;
    using SpatialOps::NO_SIDE;
    create_l_tags();
    if( boundaryType_ == NONREFLECTING ){
      switch( dir_ ){

        case XDIR:{
          switch( side_ ){
            case PLUS_SIDE:{
              typedef typename SpatialOps::OneSidedOpTypeBuilder< SpatialOps::Gradient,XMDivT,FieldT >::type XNegDivT;

              //Calculated L expressions
              typedef typename L1L2             < FieldT,  XNegDivT>::Builder L1L2;
              typedef typename L3L4L5Diagonal   < FieldT,  XNegDivT>::Builder L3L4L5Diag;
              typedef typename L3L4L5OffDiagonal< FieldT,  XNegDivT>::Builder L3L4L5OffDiag;
              typedef typename Lk               < FieldT,  XNegDivT>::Builder Lk;
              execFac.register_expression( new L1L2      ( tMgr_[L1],    mySpec_, tMgr_[P], tMgr_[RHO], tMgr_[FACEVEL], tMgr_[C], true,  tMgr_[DP], tMgr_[DVEL] ), canOverwriteExpr_, id_ );
              execFac.register_expression( new L1L2      ( tMgr_[L2],    mySpec_, tMgr_[P], tMgr_[RHO], tMgr_[FACEVEL], tMgr_[C], false, tMgr_[DP], tMgr_[DVEL] ), canOverwriteExpr_, id_ );
              execFac.register_expression( new L3L4L5Diag( tMgr_[L345D], mySpec_, tMgr_[P], tMgr_[RHO], tMgr_[FACEVEL], tMgr_[C], tMgr_[DP], tMgr_[DDENS]       ), canOverwriteExpr_, id_ );

              if( doPerp2_ ) execFac.register_expression( new L3L4L5OffDiag( tMgr_[L345O1], mySpec_, tMgr_[FACEVEL], tMgr_[W], false, tMgr_[DWVEL] ), canOverwriteExpr_, id_ );
              if( doPerp1_ ) execFac.register_expression( new L3L4L5OffDiag( tMgr_[L345O2], mySpec_, tMgr_[FACEVEL], tMgr_[V], true,  tMgr_[DVVEL]  ), canOverwriteExpr_, id_ );

              if( tMgr_.doSpecies_ ){
                for( auto i=0; i<nSpecies_-1; ++i ){
                  execFac.register_expression( new Lk( tMgr_[LKS][i], bcMask_, tMgr_[FACEVEL], tMgr_[Y_N][i], tMgr_[DIVY_N][i] ), canOverwriteExpr_, id_ );
                }
              }
              break;
            }
            case MINUS_SIDE:{
              typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Gradient,XPDivT,FieldT>::type XPosDivT;

              //Calculated L expressions
              typedef typename L1L2             < FieldT,  XPosDivT>::Builder L1L2;
              typedef typename L3L4L5Diagonal   < FieldT,  XPosDivT>::Builder L3L4L5Diag;
              typedef typename L3L4L5OffDiagonal< FieldT,  XPosDivT>::Builder L3L4L5OffDiag;
              typedef typename Lk               < FieldT,  XPosDivT>::Builder Lk;
              execFac.register_expression( new L1L2      ( tMgr_[L1],    mySpec_, tMgr_[P], tMgr_[RHO], tMgr_[FACEVEL], tMgr_[C], true,  tMgr_[DP], tMgr_[DVEL] ), canOverwriteExpr_, id_ );
              execFac.register_expression( new L1L2      ( tMgr_[L2],    mySpec_, tMgr_[P], tMgr_[RHO], tMgr_[FACEVEL], tMgr_[C], false, tMgr_[DP], tMgr_[DVEL] ), canOverwriteExpr_, id_ );
              execFac.register_expression( new L3L4L5Diag( tMgr_[L345D], mySpec_, tMgr_[P], tMgr_[RHO], tMgr_[FACEVEL], tMgr_[C], tMgr_[DP], tMgr_[DDENS]       ),        canOverwriteExpr_, id_ );

              if( doPerp2_ ) execFac.register_expression( new L3L4L5OffDiag( tMgr_[L345O1], mySpec_, tMgr_[FACEVEL], tMgr_[W], false, tMgr_[DWVEL] ), canOverwriteExpr_, id_ );
              if( doPerp1_ ) execFac.register_expression( new L3L4L5OffDiag( tMgr_[L345O2], mySpec_, tMgr_[FACEVEL], tMgr_[V], true,  tMgr_[DVVEL] ), canOverwriteExpr_, id_ );

              if( tMgr_.doSpecies_ ){
                for( auto i=0; i<nSpecies_-1; ++i ){
                  execFac.register_expression( new Lk( tMgr_[LKS][i], bcMask_, tMgr_[FACEVEL], tMgr_[Y_N][i], tMgr_[DIVY_N][i] ), canOverwriteExpr_, id_ );
                }
              }
              break;
            }
            case NO_SIDE:
              assert(false);
              break;
          } // switch( side_ )
          break;
        } // switch( dir_ ) : XDIR

        case YDIR:{
          switch( side_ ){
            case PLUS_SIDE:{
              typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Gradient,YMDivT,FieldT>::type YNegDivT;

              //Calculated L expressions
              typedef typename L1L2             < FieldT, YNegDivT>::Builder L1L2;
              typedef typename L3L4L5Diagonal   < FieldT, YNegDivT>::Builder L3L4L5Diag;
              typedef typename L3L4L5OffDiagonal< FieldT, YNegDivT>::Builder L3L4L5OffDiag;
              typedef typename Lk               < FieldT, YNegDivT>::Builder Lk;
              execFac.register_expression( new L1L2      ( tMgr_[L1],    mySpec_, tMgr_[P], tMgr_[RHO], tMgr_[FACEVEL], tMgr_[C], true,  tMgr_[DP], tMgr_[DVEL] ), canOverwriteExpr_, id_ );
              execFac.register_expression( new L1L2      ( tMgr_[L2],    mySpec_, tMgr_[P], tMgr_[RHO], tMgr_[FACEVEL], tMgr_[C], false, tMgr_[DP], tMgr_[DVEL] ), canOverwriteExpr_, id_ );
              execFac.register_expression( new L3L4L5Diag( tMgr_[L345D], mySpec_, tMgr_[P], tMgr_[RHO], tMgr_[FACEVEL], tMgr_[C], tMgr_[DP], tMgr_[DDENS]       ), canOverwriteExpr_, id_ );

              if( doPerp2_ ) execFac.register_expression( new L3L4L5OffDiag( tMgr_[L345O1], mySpec_, tMgr_[FACEVEL], tMgr_[W], true,  tMgr_[DWVEL] ), canOverwriteExpr_, id_ );
              if( doPerp1_ ) execFac.register_expression( new L3L4L5OffDiag( tMgr_[L345O2], mySpec_, tMgr_[FACEVEL], tMgr_[U], false, tMgr_[DUVEL] ), canOverwriteExpr_, id_ );

              if( tMgr_.doSpecies_ ){
                for( auto i=0; i<nSpecies_-1; ++i ){
                  execFac.register_expression( new Lk( tMgr_[LKS][i], bcMask_, tMgr_[FACEVEL], tMgr_[Y_N][i], tMgr_[DIVY_N][i] ), canOverwriteExpr_, id_ );
                }
              }
              break;
            }
            case MINUS_SIDE:{
              typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Gradient,YPDivT,FieldT>::type YPosDivT;

              //Calculated L expressions
              typedef typename L1L2             < FieldT,  YPosDivT>::Builder L1L2;
              typedef typename L3L4L5Diagonal   < FieldT,  YPosDivT>::Builder L3L4L5Diag;
              typedef typename L3L4L5OffDiagonal< FieldT,  YPosDivT>::Builder L3L4L5OffDiag;
              typedef typename Lk               < FieldT,  YPosDivT>::Builder Lk;
              execFac.register_expression( new L1L2      ( tMgr_[L1],    mySpec_, tMgr_[P], tMgr_[RHO], tMgr_[FACEVEL], tMgr_[C], true,  tMgr_[DP], tMgr_[DVEL]  ), canOverwriteExpr_, id_ );
              execFac.register_expression( new L1L2      ( tMgr_[L2],    mySpec_, tMgr_[P], tMgr_[RHO], tMgr_[FACEVEL], tMgr_[C], false, tMgr_[DP], tMgr_[DVEL] ), canOverwriteExpr_, id_ );
              execFac.register_expression( new L3L4L5Diag( tMgr_[L345D], mySpec_, tMgr_[P], tMgr_[RHO], tMgr_[FACEVEL], tMgr_[C], tMgr_[DP], tMgr_[DDENS]       ), canOverwriteExpr_, id_ );

              if( doPerp2_ ) execFac.register_expression( new L3L4L5OffDiag( tMgr_[L345O1], mySpec_, tMgr_[FACEVEL], tMgr_[W], true,  tMgr_[DWVEL] ), canOverwriteExpr_, id_ );
              if( doPerp1_ ) execFac.register_expression( new L3L4L5OffDiag( tMgr_[L345O2], mySpec_, tMgr_[FACEVEL], tMgr_[U], false, tMgr_[DUVEL] ), canOverwriteExpr_, id_ );

              if( tMgr_.doSpecies_ ){
                for( auto i=0; i<nSpecies_-1; ++i ){
                  execFac.register_expression( new Lk( tMgr_[LKS][i], bcMask_, tMgr_[FACEVEL], tMgr_[Y_N][i], tMgr_[DIVY_N][i] ), canOverwriteExpr_, id_ );
                }
              }
              break;
            }
            case NO_SIDE:
              assert(false);
              break;
          } // switch( side_ )
          break;
        } // switch( dir_ ) : YDIR

        case ZDIR:{
          switch( side_ ){
            case PLUS_SIDE:{
              typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Gradient,ZMDivT,FieldT>::type ZNegDivT;

              //Calculated L expressions
              typedef typename L1L2             < FieldT,  ZNegDivT>::Builder L1L2;
              typedef typename L3L4L5Diagonal   < FieldT,  ZNegDivT>::Builder L3L4L5Diag;
              typedef typename L3L4L5OffDiagonal< FieldT,  ZNegDivT>::Builder L3L4L5OffDiag;
              typedef typename Lk               < FieldT,  ZNegDivT>::Builder Lk;
              execFac.register_expression( new L1L2      ( tMgr_[L1],    mySpec_, tMgr_[P], tMgr_[RHO], tMgr_[FACEVEL], tMgr_[C], true,  tMgr_[DP], tMgr_[DVEL] ), canOverwriteExpr_, id_ );
              execFac.register_expression( new L1L2      ( tMgr_[L2],    mySpec_, tMgr_[P], tMgr_[RHO], tMgr_[FACEVEL], tMgr_[C], false, tMgr_[DP], tMgr_[DVEL] ), canOverwriteExpr_, id_ );
              execFac.register_expression( new L3L4L5Diag( tMgr_[L345D], mySpec_, tMgr_[P], tMgr_[RHO], tMgr_[FACEVEL], tMgr_[C], tMgr_[DP], tMgr_[DDENS]       ), canOverwriteExpr_, id_ );

              if( doPerp2_ ) execFac.register_expression( new L3L4L5OffDiag( tMgr_[L345O1], mySpec_, tMgr_[FACEVEL], tMgr_[V], false, tMgr_[DVVEL] ), canOverwriteExpr_, id_ );
              if( doPerp1_ ) execFac.register_expression( new L3L4L5OffDiag( tMgr_[L345O2], mySpec_, tMgr_[FACEVEL], tMgr_[U], true, tMgr_[DUVEL]  ), canOverwriteExpr_, id_ );

              if( tMgr_.doSpecies_ ){
                for( auto i=0; i<nSpecies_-1; ++i ){
                  execFac.register_expression( new Lk( tMgr_[LKS][i], bcMask_, tMgr_[FACEVEL], tMgr_[Y_N][i], tMgr_[DIVY_N][i] ), canOverwriteExpr_, id_ );
                }
              }
              break;
            }
            case MINUS_SIDE:{
              typedef typename SpatialOps::OneSidedOpTypeBuilder<SpatialOps::Gradient,ZPDivT,FieldT>::type ZPosDivT;

              //Calculated L expressions
              typedef typename L1L2             < FieldT,  ZPosDivT>::Builder L1L2;
              typedef typename L3L4L5Diagonal   < FieldT,  ZPosDivT>::Builder L3L4L5Diag;
              typedef typename L3L4L5OffDiagonal< FieldT,  ZPosDivT>::Builder L3L4L5OffDiag;
              typedef typename Lk               < FieldT,  ZPosDivT>::Builder Lk;
              execFac.register_expression( new L1L2      ( tMgr_[L1],    mySpec_, tMgr_[P], tMgr_[RHO], tMgr_[FACEVEL], tMgr_[C], true,  tMgr_[DP], tMgr_[DVEL] ), canOverwriteExpr_, id_ );
              execFac.register_expression( new L1L2      ( tMgr_[L2],    mySpec_, tMgr_[P], tMgr_[RHO], tMgr_[FACEVEL], tMgr_[C], false, tMgr_[DP], tMgr_[DVEL] ), canOverwriteExpr_, id_ );
              execFac.register_expression( new L3L4L5Diag( tMgr_[L345D], mySpec_, tMgr_[P], tMgr_[RHO], tMgr_[FACEVEL], tMgr_[C], tMgr_[DP], tMgr_[DDENS]       ), canOverwriteExpr_, id_ );

              if( doPerp2_ ) execFac.register_expression( new L3L4L5OffDiag( tMgr_[L345O1], mySpec_, tMgr_[FACEVEL], tMgr_[V], false, tMgr_[DVVEL] ), canOverwriteExpr_, id_ );
              if( doPerp1_ ) execFac.register_expression( new L3L4L5OffDiag( tMgr_[L345O2], mySpec_, tMgr_[FACEVEL], tMgr_[U], true,  tMgr_[DUVEL] ), canOverwriteExpr_, id_ );

              if( tMgr_.doSpecies_ ){
                for( auto i=0; i<nSpecies_-1; ++i ){
                  execFac.register_expression( new Lk( tMgr_[LKS][i], bcMask_, tMgr_[FACEVEL], tMgr_[Y_N][i], tMgr_[DIVY_N][i] ), canOverwriteExpr_, id_ );
                }
              }
              break;
            } // case MINUS_SIDE
            case NO_SIDE:
              assert(false);
              break;
          } // switch( side_ )
          break;
        } // case ZDIR

        case NODIR:{
          std::ostringstream msg;
          msg << __FILE__ << " : " << __LINE__ << std::endl
              << "No DIR is assigned in the BCBuilder" << std::endl;
          throw std::runtime_error( msg.str() );
        }
      } // switch( dir_ )
    }//Boundary Type
  }

  template< typename FieldT >
  void
  BCBuilder<FieldT>::register_l_set( Expr::ExpressionFactory& execFac )
  {
    //Register the source term for pressure
    typedef typename RPressure<FieldT>::Builder RPressure;
    if( tMgr_.doSpecies_ ){
      execFac.register_expression( new RPressure( tMgr_[RP], bcMask_, tMgr_[T], tMgr_[CP], tMgr_[CV], tMgr_[MMW], tMgr_[H_N], tMgr_[R_N], specMW_, gasConstant_ ),
                                   canOverwriteExpr_, id_ );
    }
    else{
      execFac.register_expression( new RPressure( tMgr_[RP], bcMask_ ), canOverwriteExpr_, id_ );
    }

    switch( boundaryType_ ){
      case NONREFLECTING:{
        typedef typename L1Set         <FieldT>::Builder L1Set;
        typedef typename L345Set       <FieldT>::Builder L345Set;
        typedef typename L345OffDiagSet<FieldT>::Builder L345OffDiagSet;
        execFac.register_expression( new L1Set( tMgr_[L1_M], mySpec_, tMgr_[P], tMgr_[FACEVEL], tMgr_[C], tMgr_[RP] ), canOverwriteExpr_, id_ );
        execFac.register_expression( new L345Set( tMgr_[L345D_M], mySpec_, tMgr_[RP], tMgr_[L345D],  tMgr_[FACEVEL], tMgr_[C] ), canOverwriteExpr_, id_ );
        if( doPerp2_ ) execFac.register_expression( new L345OffDiagSet( tMgr_[L345O1_M], mySpec_, tMgr_[L345O1], tMgr_[FACEVEL] ), canOverwriteExpr_, id_ );
        if( doPerp1_ ) execFac.register_expression( new L345OffDiagSet( tMgr_[L345O2_M], mySpec_, tMgr_[L345O2], tMgr_[FACEVEL] ), canOverwriteExpr_, id_ );
        if( tMgr_.doSpecies_ ){
          typedef typename LkSet<FieldT>::Builder LkSet;
          for( auto i=0; i<nSpecies_-1; ++i ){
            execFac.register_expression( new LkSet( tMgr_[LKS_M][i], mySpec_, tMgr_[LKS][i], tMgr_[FACEVEL] ), canOverwriteExpr_, id_ );
          }
        }
        break;
      }
      case HARD_INFLOW:
        typedef typename L1L2HardInflow<FieldT>::Builder L1L2;
        execFac.register_expression( new L1L2( tMgr_[L1_M], mySpec_, tMgr_[C], tMgr_[RHO], tMgr_[FACEVEL], tMgr_[DPHARDINFLOW], tMgr_[DVELHARDINFLOW], true  ), canOverwriteExpr_, id_ );
        execFac.register_expression( new L1L2( tMgr_[L2_M], mySpec_, tMgr_[C], tMgr_[RHO], tMgr_[FACEVEL], tMgr_[DPHARDINFLOW], tMgr_[DVELHARDINFLOW], false ), canOverwriteExpr_, id_ );
        break;
      case WALL:
        break;
      default:
        std::ostringstream msg;
        msg << __FILE__ << " : " << __LINE__ << std::endl
            << "No NSCBC type is assigned in the BCBuilder, given type is:  "
            << type_string(boundaryType_) << std::endl;
        throw std::runtime_error( msg.str() );
    }
  }
} // namespace NSCBC

#endif /* CHARACTERISTICBCBUILDER_H_ */
