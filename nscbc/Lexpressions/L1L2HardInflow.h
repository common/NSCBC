/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   L1L2HardInflow.h
 *  \date   Created on: Mar 20, 2015
 *  \author Author: Derek Cline
 *
 */

#ifndef L1L2HardInflow_h
#define L1L2HardInflow_h

#include <expression/ExprLib.h>

/**
 *  \class L1L2HardInflow
 *  \brief An expression for Navier-Stokes Characteristic Boundary
 *         Condition Calculations in the ODT code database
 *         See Sutherland and Kennedy 2003 or Coussement et al. 2012
 *         for formulations
 *         L1 and L2 represent the acoustic waves of the Navier Stokes Eqns.
 *         This should utilize set primitives to calculate an incoming wave
 *         for the use in setting the density RHS.
 *
 */
template< typename FieldT >
class L1L2HardInflow : public Expr::Expression<FieldT>
{
  DECLARE_FIELDS( FieldT, c_, density_, vel_, divPress_, divVel_ )

  const NSCBC::NSCBCInfo<FieldT> info_;
  const SpatialOps::SpatialMask<FieldT> mask_;
  const bool isL1_;
  double sign_;

  L1L2HardInflow( const NSCBC::NSCBCInfo<FieldT>& info,
                  const Expr::Tag& cTag,
                  const Expr::Tag& densityTag,
                  const Expr::Tag& velTag,
                  const Expr::Tag& divPressTag,
                  const Expr::Tag& divVelTag,
                  const bool isL1 );

  ~L1L2HardInflow(){}

public:

  void evaluate();

  class Builder : public Expr::ExpressionBuilder
  {
    const NSCBC::NSCBCInfo<FieldT> info_;
    const Expr::Tag cT_,densityT_,velT_,divPressT_,divVelT_;
    const bool isL1_;
  public:
    Builder( const Expr::Tag& result,
             const NSCBC::NSCBCInfo<FieldT>& info,
             const Expr::Tag& cTag,
             const Expr::Tag& densityTag,
             const Expr::Tag& velTag,
             const Expr::Tag& divPressTag,
             const Expr::Tag& divVelTag,
             const bool isL1 )
    : ExpressionBuilder(result),
      info_     ( info        ),
      cT_       ( cTag        ),
      densityT_ ( densityTag  ),
      velT_     ( velTag      ),
      divPressT_( divPressTag ),
      divVelT_  ( divVelTag   ),
      isL1_     ( isL1        )
    {}

    Expr::ExpressionBase* build() const{
      return new L1L2HardInflow<FieldT>(info_,cT_,densityT_,velT_,divPressT_,divVelT_,isL1_);
    }
  };
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################
template< typename FieldT >
L1L2HardInflow<FieldT>::
L1L2HardInflow( const NSCBC::NSCBCInfo<FieldT>& info,
                const Expr::Tag& cTag,
                const Expr::Tag& densityTag,
                const Expr::Tag& velTag,
                const Expr::Tag& divPressTag,
                const Expr::Tag& divVelTag,
                const bool isL1 )
  : Expr::Expression<FieldT>(),
    info_( info       ),
    mask_( info_.mask ),
    isL1_( isL1       )
{
  this->set_gpu_runnable( true );

  c_        = this->template create_field_request<FieldT>( cTag        );
  density_  = this->template create_field_request<FieldT>( densityTag  );
  vel_      = this->template create_field_request<FieldT>( velTag      );
  divPress_ = this->template create_field_request<FieldT>( divPressTag );
  divVel_   = this->template create_field_request<FieldT>( divVelTag   );

  sign_ = isL1_ ? 1.0 : -1.0;
  // grabs a -1 or 1 for checking whether we have inflow or outflow
  switch( info_.side ){
    case SpatialOps::PLUS_SIDE:
      sign_ *= 1.0;
      break;
    case SpatialOps::MINUS_SIDE:
      sign_ *= -1.0;
      break;
    default:
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "Invalid side in L1L2HardInflow Calculated expression in NSCBC" << std::endl;
      throw std::runtime_error( msg.str() );
  }
}

//--------------------------------------------------------------------

template< typename FieldT >
void
L1L2HardInflow<FieldT>::
evaluate()
{
  using namespace SpatialOps;

  FieldT& result = this->value();

  const FieldT& c        = c_       ->field_ref();
  const FieldT& density  = density_ ->field_ref();
  const FieldT& vel      = vel_     ->field_ref();
  const FieldT& divPress = divPress_->field_ref();
  const FieldT& divVel   = divVel_  ->field_ref();

  masked_assign( mask_, result, ( vel - sign_ * c ) * ( divPress - sign_ * density * c * divVel ) / 2);
}

//--------------------------------------------------------------------

#endif /* L1L2HardInflow_h */
