/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *      \file   L1Set.h
 *      \date   Created on: Mar 20, 2015
 *      \author Author: Derek Cline
 *
 *      \class L1Set
 *      \brief An expression for Navier-Stokes Characteristic Boundary
 *             Condition Calculations in the ODT code database
 *             See Sutherland and Kennedy 2003 or Coussement et al. 2012
 *             for formulations
 *             This sets the L1 wave amplitude to a pressure relaxation term
 *             on an outflow and source term on an inflow.
 *
 *      \param info Struct containing various NSCBC info (side, type, etc).
 *      \param pressureTag Tag corresponding to the pressure
 *      \param velTag Tag corresponding to the velocity
 *      \param cTag Tag corresponding to the speed of sound
 *      \param RpTag Tag corresponding to the Characteristic Pressure Source Term
 *      \param isL1 a boolean that should be set to false if we require the
 *             L2 expression (they are the same other than a sign change)
 *      \tparam FieldT Volume Field Type
 */

#ifndef L1Set_h
#define L1Set_h

#include <expression/ExprLib.h>

template< typename FieldT >
class L1Set : public Expr::Expression<FieldT>
{
  DECLARE_FIELDS( FieldT, pressure_, vel_, c_, Rp_ )

  const NSCBC::NSCBCInfo<FieldT> info_;
  double side_;

  L1Set( const NSCBC::NSCBCInfo<FieldT>& info,
         const Expr::Tag& pressureTag,
         const Expr::Tag& velTag,
         const Expr::Tag& cTag,
         const Expr::Tag& RpTag );

  ~L1Set(){}

public:

  void evaluate();

  class Builder : public Expr::ExpressionBuilder
  {
    const NSCBC::NSCBCInfo<FieldT> info_;
    const Expr::Tag pressureT_, velT_, cT_, RpT_;

  public:
    Builder( const Expr::Tag& result,
             const NSCBC::NSCBCInfo<FieldT>& info,
             const Expr::Tag& pressureTag,
             const Expr::Tag& velTag,
             const Expr::Tag& cTag,
             const Expr::Tag& RpTag )
    : ExpressionBuilder(result),
      info_     ( info        ),
      pressureT_( pressureTag ),
      velT_     ( velTag      ),
      cT_       ( cTag        ),
      RpT_      ( RpTag       )
    {}

    Expr::ExpressionBase* build() const{
      return new L1Set<FieldT>(info_,pressureT_,velT_,cT_,RpT_);
    }

  };
};

// ###################################################################
//
//                          Implementation
//
// ###################################################################

template< typename FieldT >
L1Set<FieldT>::
L1Set( const NSCBC::NSCBCInfo<FieldT>& info,
       const Expr::Tag& pressureTag,
       const Expr::Tag& velTag,
       const Expr::Tag& cTag,
       const Expr::Tag& RpTag )
  : Expr::Expression<FieldT>(),
    info_( info )
{
  this->set_gpu_runnable( true );

  Rp_      = this->template create_field_request<FieldT>( RpTag       );
  c_       = this->template create_field_request<FieldT>( cTag        );
  pressure_= this->template create_field_request<FieldT>( pressureTag );
  vel_     = this->template create_field_request<FieldT>( velTag      );

  // grabs a -1 or 1 for checking whether we have inflow or outflow
  switch( info_.side ){
    case SpatialOps::PLUS_SIDE:
      side_ = 1.0;
      break;
    case SpatialOps::MINUS_SIDE:
      side_ = -1.0;
      break;
    default:
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "Invalid side in L345Set expression in NSCBC" << std::endl;
      throw std::runtime_error( msg.str() );
  }
}

//--------------------------------------------------------------------

template< typename FieldT >
void
L1Set<FieldT>::
evaluate()
{
  using namespace SpatialOps;

  FieldT& result = this->value();

  const FieldT& c        = c_       ->field_ref();
  const FieldT& Rp       = Rp_      ->field_ref();
  const FieldT& pressure = pressure_->field_ref();
  const FieldT& vel      = vel_     ->field_ref();

  //Rudy and Strikwerda (1980) or Sutherland (2003)
  const double relaxation = 0.287;

  SpatFldPtr<FieldT> Ma   = SpatialFieldStore::get<FieldT>( result );

  masked_assign( info_.mask, *Ma, abs(vel)  / c );

  result <<= cond( (side_ * vel) > 0.0, (Rp / 2.0) + relaxation * c * ( (1 - *Ma * *Ma ) / ( 2 * info_.domainlength ) ) * ( pressure - info_.farfieldpressure ) )
                 (                       Rp / 2.0);
}

//--------------------------------------------------------------------

#endif /* L1Set_h */
