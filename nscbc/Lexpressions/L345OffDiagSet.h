/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */


/**
 *      \file   L345OffDiagSet.h
 *      \date   Created on: Mar 20, 2015
 *      \author Author: Derek Cline
 *
 *      \class L345OffDiagSet
 *      \brief L345OffDiagSet an expression for Navier-Stokes Characteristic Boundary
 *             Condition Calculations in the ODT code database
 *             See Sutherland and Kennedy 2003 or Coussement et al. 2012
 *             for formulations
 *
 *      \param info Struct containing various NSCBC info (side, type, etc).
 *      \param L345Tag Tag corresponding to the L345 Calculated Wave
 *      \param velTag Tag corresponding to the velocity normal to the boundary
 *      \tparam FieldT Volume Field Type
 *
 */

#ifndef L345OffDiagSet_h
#define L345OffDiagSet_h

#include <expression/ExprLib.h>

#include <spatialops/Nebo.h>

template< typename FieldT >
class L345OffDiagSet : public Expr::Expression<FieldT>
{
  DECLARE_FIELDS( FieldT, L345_, vel_ )

  L345OffDiagSet( const NSCBC::NSCBCInfo<FieldT>& info,
                  const Expr::Tag& L345Tag,
                  const Expr::Tag& velTag );

  ~L345OffDiagSet(){}

public:

  void evaluate();

  class Builder : public Expr::ExpressionBuilder
  {
    const NSCBC::NSCBCInfo<FieldT> info_;
    const Expr::Tag L345T_, velT_;

  public:
    Builder( const Expr::Tag& result,
             const NSCBC::NSCBCInfo<FieldT>& info,
             const Expr::Tag& L345Tag,
             const Expr::Tag& velTag )
    : ExpressionBuilder(result),
      info_ ( info    ),
      L345T_( L345Tag ),
      velT_ ( velTag  )
    {}

    Expr::ExpressionBase* build() const{
      return new L345OffDiagSet<FieldT>(info_,L345T_,velT_);
    }

  };

private:
  const NSCBC::NSCBCInfo<FieldT> info_;
  double side_;
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################


template< typename FieldT >
L345OffDiagSet<FieldT>::
L345OffDiagSet( const NSCBC::NSCBCInfo<FieldT>& info,
                const Expr::Tag& L345Tag,
                const Expr::Tag& velTag )
  : Expr::Expression<FieldT>(),
    info_( info )
{
  this->set_gpu_runnable( true );

  L345_ = this->template create_field_request<FieldT>( L345Tag );
  vel_  = this->template create_field_request<FieldT>( velTag  );

  // grabs a -1 or 1 for checking whether we have inflow or outflow
  switch( info_.side ){
    case SpatialOps::PLUS_SIDE:
      side_ = 1.0;
      break;
    case SpatialOps::MINUS_SIDE:
      side_ = -1.0;
      break;
    default:
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "Invalid side in L345OffDiagSet expression in NSCBC" << std::endl;
      throw std::runtime_error( msg.str() );
  }
}

//--------------------------------------------------------------------

template< typename FieldT >
void
L345OffDiagSet<FieldT>::
evaluate()
{
  using namespace SpatialOps;

  FieldT& result = this->value();

  const FieldT& L345 = L345_->field_ref();
  const FieldT& vel  = vel_ ->field_ref();

  result <<= cond( (side_ * vel) > 0.0, L345 )
                 (                      0.0  );
}

//--------------------------------------------------------------------

#endif /* L345OffDiagSet_h */
