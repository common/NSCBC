/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *      \file   L345Set.h
 *      \date   Created on: Mar 20, 2015
 *      \author Author: Derek Cline
 *
 *      \class L345Set
 *      \brief L345Set an expression for Navier-Stokes Characteristic Boundary
 *             Condition Calculations in the ODT code database
 *             See Sutherland and Kennedy 2003 or Coussement et al. 2012
 *             for formulations
 *             This will set the L3, L4 and L5 expressions.  This will maintain
 *             the original L345 values for an outflow.
 *
 *      \param info Struct containing various NSCBC info (side, type, etc).
 *      \param RpTag Tag corresponding to the Characteristic Pressure Source Term
 *      \param L345Tag Tag corresponding to the L345 Calculated Wave
 *      \param velTag Tag corresponding to the velocity
 *      \param cTag Tag corresponding to the speed of sound
 *      \param isL1 a boolean that should be set to false if we require the
 *             L2 expression (they are the same other than a sign change)
 *      \tparam FieldT Volume Field Type
 *
 */

#ifndef L345Set_h
#define L345Set_h

#include <expression/ExprLib.h>

template< typename FieldT >
class L345Set : public Expr::Expression<FieldT>
{
  DECLARE_FIELDS( FieldT, Rp_, L345_, vel_, c_ )

  L345Set( const NSCBC::NSCBCInfo<FieldT>& info,
           const Expr::Tag& RpTag,
           const Expr::Tag& L345Tag,
           const Expr::Tag& velTag,
           const Expr::Tag& cTag );

  ~L345Set(){}

public:

  void evaluate();

  class Builder : public Expr::ExpressionBuilder
  {
    const NSCBC::NSCBCInfo<FieldT> info_;
    const Expr::Tag RpT_, L345T_, velT_, cT_;

  public:
    Builder( const Expr::Tag& result,
             const NSCBC::NSCBCInfo<FieldT>& info,
             const Expr::Tag& RpTag,
             const Expr::Tag& L345Tag,
             const Expr::Tag& velTag,
             const Expr::Tag& cTag )
    : ExpressionBuilder(result),
      info_ ( info    ),
      RpT_  ( RpTag   ),
      L345T_( L345Tag ),
      velT_ ( velTag  ),
      cT_   ( cTag    )
    {}

    Expr::ExpressionBase* build() const{
      return new L345Set<FieldT>(info_,RpT_,L345T_,velT_, cT_ );
    }

  };

private:
  const NSCBC::NSCBCInfo<FieldT> info_;
  double side_;
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################


template< typename FieldT >
L345Set<FieldT>::
L345Set( const NSCBC::NSCBCInfo<FieldT>& info,
         const Expr::Tag& RpTag,
         const Expr::Tag& L345Tag,
         const Expr::Tag& velTag,
         const Expr::Tag& cTag )
  : Expr::Expression<FieldT>(),
    info_( info )
{
  this->set_gpu_runnable( true );

  Rp_   = this->template create_field_request<FieldT>( RpTag   );
  L345_ = this->template create_field_request<FieldT>( L345Tag );
  vel_  = this->template create_field_request<FieldT>( velTag  );
  c_    = this->template create_field_request<FieldT>( cTag    );

  // grabs a -1 or 1 for checking whether we have inflow or outflow
  switch( info_.side ){
    case SpatialOps::PLUS_SIDE : side_ =  1.0; break;
    case SpatialOps::MINUS_SIDE: side_ = -1.0; break;
    default:
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "Invalid side in L345Set expression in NSCBC" << std::endl;
      throw std::runtime_error( msg.str() );
  }
}

//--------------------------------------------------------------------

template< typename FieldT >
void
L345Set<FieldT>::
evaluate()
{
  using namespace SpatialOps;

  FieldT& result = this->value();

  const FieldT& vel  = vel_ ->field_ref();
  const FieldT& c    = c_   ->field_ref();
  const FieldT& L345 = L345_->field_ref();
  const FieldT& Rp   = Rp_  ->field_ref();

  //We maintain L345 for an outflow but set it for an inflow
  result <<= cond( (side_ * vel) > 0.0, L345                        )
                 (                      -1.0 * side_ * Rp / (c * c) );
}

//--------------------------------------------------------------------

#endif /* L345Set_h */
