/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *      \file   L3L4L5Diagonal.h
 *      \date   Created on: Mar 20, 2015
 *      \author Author: Derek Cline
 *
 *      \class L3L4L5Diagonal
 *      \brief L3L4L5Diagonal an expression for Navier-Stokes Characteristic Boundary
 *             Condition Calculations in the ODT code database
 *             See Sutherland and Kennedy 2003 or Coussement et al. 2012
 *             for formulations
 *             This term represents the entropy waves. L3, L4 and L5 will be normal
 *             to the boundary.  That is, this expression will calculate L3 for an
 *             X-Boundary, L4 for a Y-Boundary and L5 for a Z-Boundary.
 *
 *      \tparam FieldT Volume Field Type
 *      \tparam DivT this template parameter should be a one sided stencil
 *              corresponding to the direction of the boundary (plus or minus)
 *              NOTE: A plus face boundary requires a minus direction stencil
 *      \param pressureTag Tag corresponding to the pressure
 *      \param densityTag Tag corresponding to the density
 *      \param velTag Tag corresponding to the velocity
 *      \param cTag Tag corresponding to the speed of sound
 */

#ifndef L3L4L5Diagonal_h
#define L3L4L5Diagonal_h

#include <expression/ExprLib.h>

template< typename FieldT, typename DivT >
class L3L4L5Diagonal
  : public Expr::Expression<FieldT>
{
  DECLARE_FIELDS( FieldT, pressure_, density_, vel_, c_, divPress_, divDens_ )

  const NSCBC::NSCBCInfo<FieldT> info_;
  const SpatialOps::SpatialMask<FieldT> mask_;
  const bool doDiv_;
  const DivT* divOp_;
  double sign_;

  L3L4L5Diagonal( const NSCBC::NSCBCInfo<FieldT>& info,
                  const Expr::Tag& pressureTag,
                  const Expr::Tag& densityTag,
                  const Expr::Tag& velTag,
                  const Expr::Tag& cTag,
                  const Expr::Tag& divPressTag,
                  const Expr::Tag& divDensTag );

  ~L3L4L5Diagonal(){}

public:

  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();

  class Builder : public Expr::ExpressionBuilder
  {
    const NSCBC::NSCBCInfo<FieldT> info_;
    const Expr::Tag pressureT_,densityT_,velT_,cT_,divPressT_,divDensT_;
  public:
    Builder( const Expr::Tag& result,
             const NSCBC::NSCBCInfo<FieldT>& info,
             const Expr::Tag& pressureTag,
             const Expr::Tag& densityTag,
             const Expr::Tag& velTag,
             const Expr::Tag& cTag,
             const Expr::Tag& divPressTag,
             const Expr::Tag& divDensTag )
    : ExpressionBuilder(result),
      info_      ( info        ),
      pressureT_ ( pressureTag ),
      densityT_  ( densityTag  ),
      velT_      ( velTag      ),
      cT_        ( cTag        ),
      divPressT_ ( divPressTag ),
      divDensT_  ( divDensTag  )
    {}

    Expr::ExpressionBase* build() const{
      return new L3L4L5Diagonal<FieldT,DivT>(info_,pressureT_,densityT_,velT_,cT_,divPressT_,divDensT_);
    }

  };

};



// ###################################################################
//
//                          Implementation
//
// ###################################################################
template< typename FieldT, typename DivT >
L3L4L5Diagonal<FieldT,DivT>::
L3L4L5Diagonal( const NSCBC::NSCBCInfo<FieldT>& info,
                const Expr::Tag& pressureTag,
                const Expr::Tag& densityTag,
                const Expr::Tag& velTag,
                const Expr::Tag& cTag,
                const Expr::Tag& divPressTag,
                const Expr::Tag& divDensTag )
  : Expr::Expression<FieldT>(),
    info_( info       ),
    mask_( info_.mask ),
    doDiv_( divDensTag == Expr::Tag() )
{
  this->set_gpu_runnable( true );

  pressure_ = this->template create_field_request<FieldT>( pressureTag );
  density_  = this->template create_field_request<FieldT>( densityTag  );
  vel_      = this->template create_field_request<FieldT>( velTag      );
  c_        = this->template create_field_request<FieldT>( cTag        );

  sign_ = 0.0;

  // grabs a -1 or 1 for checking whether we have inflow or outflow
  switch( info_.side ){
    case SpatialOps::PLUS_SIDE : sign_ =  1.0; break;
    case SpatialOps::MINUS_SIDE: sign_ = -1.0; break;
    default:
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "Invalid side in L345Calc expression in NSCBC" << std::endl;
      throw std::runtime_error( msg.str() );
  }

  if( !doDiv_ ){
    assert( divPressTag != Expr::Tag() );
    divPress_ = this->template create_field_request<FieldT>( divPressTag );
    divDens_  = this->template create_field_request<FieldT>( divDensTag  );
  }
}

//--------------------------------------------------------------------

template< typename FieldT, typename DivT >
void
L3L4L5Diagonal<FieldT,DivT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  divOp_ = opDB.retrieve_operator<DivT>();
}

//--------------------------------------------------------------------

template< typename FieldT, typename DivT >
void
L3L4L5Diagonal<FieldT,DivT>::
evaluate()
{
  using namespace SpatialOps;

  FieldT& result = this->value();

  const FieldT& c        = c_       ->field_ref();
  const FieldT& pressure = pressure_->field_ref();
  const FieldT& density  = density_ ->field_ref();
  const FieldT& vel      = vel_     ->field_ref();

  if( doDiv_ ) masked_assign( mask_, result, sign_ * ( vel ) * ( (*divOp_)(density) - (1 / (c * c) ) * (*divOp_)(pressure) ) );
  else{
    const FieldT& divPress = divPress_->field_ref();
    const FieldT& divDens  = divDens_ ->field_ref();

    masked_assign( mask_, result, sign_ * ( vel ) * ( divDens - (1 / (c * c) ) * divPress ) );
  }
}

//--------------------------------------------------------------------

#endif /* L3L4L5Diagonal_h */
