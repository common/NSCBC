/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *      \file   L3L4L5OffDiagonal.h
 *      \date   Created on: Mar 20, 2015
 *      \author Author: Derek Cline
 *
 *      \class L3L4L5OffDiagonal
 *      \brief L3L4L5OffDiagonal an expression for Navier-Stokes Characteristic Boundary
 *             Condition Calculations in the ODT code database
 *             See Sutherland and Kennedy 2003 or Coussement et al. 2012
 *             for formulations
 *             This term represent the vorticity waves.  This will be perpendicular to a
 *             wall.  For example, on an X-Boundary, this expression will calculate L4 and L5,
 *             on a Y-Boundary, L3 and L5 and on a Z-Boundary, L3 and L4.
 *
 *      \param positiveCurl is a boolean that should be set to false
 *             if our velocities rotate around a negative curl (right hand rule).
 *             For example, if vel = x and velgrad = z, the positiveCurl should be false
 *             but if vel = y and velgrad = z, then we follow the right hand rule and
 *             this variable should be set to true
 *             L2 expression (they are the same other than a sign change)
 *      \param velTag  Velocity field that corresponds to the L term (i.e. NOT normal to the boundary)
 *      \param velGrad should be a velocity field that is GOING to be operated on,
 *             (e.g. x-velocity) and a volume field, NOT the gradient.  The gradient will be taken in
 *             the evaluate function of this expression.
 *      \tparam FieldT Volume Field Type
 *      \tparam DivT this template parameter should be a one sided stencil
 *              corresponding to the direction of the boundary (plus or minus)
 *              NOTE: A plus face boundary requires a minus direction stencil
 *
 */

#ifndef L3L4L5OffDiagonal_h
#define L3L4L5OffDiagonal_h

#include <expression/ExprLib.h>

template< typename FieldT, typename DivT >
class L3L4L5OffDiagonal
  : public Expr::Expression<FieldT>
{
  DECLARE_FIELDS( FieldT, vel_, velGrad_, divVel_ )

  L3L4L5OffDiagonal( const NSCBC::NSCBCInfo<FieldT>& info,
                     const Expr::Tag& velTag,
                     const Expr::Tag& velGradTag,
                     const bool positiveCurl,
                     const Expr::Tag& divVelTag );

  ~L3L4L5OffDiagonal(){}

public:

  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();

  class Builder : public Expr::ExpressionBuilder
  {
    const NSCBC::NSCBCInfo<FieldT> info_;
    const Expr::Tag velT_,velGradT_,divVelT_;
    const bool positiveCurl_;
  public:
    Builder( const Expr::Tag& result,
             const NSCBC::NSCBCInfo<FieldT>& info,
             const Expr::Tag& velTag,
             const Expr::Tag& velGradTag,
             const bool positiveCurl,
             const Expr::Tag& divVelTag )
    : ExpressionBuilder(result),
      info_        ( info         ),
      velT_        ( velTag       ),
      velGradT_    ( velGradTag   ),
      divVelT_     ( divVelTag    ),
      positiveCurl_( positiveCurl )
    {}

    Expr::ExpressionBase* build() const{
      return new L3L4L5OffDiagonal<FieldT,DivT>(info_,velT_,velGradT_,positiveCurl_,divVelT_);
    }

  };

private:
  const NSCBC::NSCBCInfo<FieldT> info_;
  const SpatialOps::SpatialMask<FieldT> mask_;
  const bool positiveCurl_;
  const bool doDiv_;
  double sign_;
  const DivT* divOp_;
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################
template< typename FieldT, typename DivT >
L3L4L5OffDiagonal<FieldT,DivT>::
L3L4L5OffDiagonal( const NSCBC::NSCBCInfo<FieldT>& info,
                   const Expr::Tag& velTag,
                   const Expr::Tag& velGradTag,
                   const bool positiveCurl,
                   const Expr::Tag& divVelTag )
  : Expr::Expression<FieldT>(),
    info_        ( info         ),
    mask_        ( info_.mask   ),
    positiveCurl_( positiveCurl ),
    doDiv_( divVelTag == Expr::Tag() )
{
  this->set_gpu_runnable( true );

  vel_         = this->template create_field_request<FieldT>( velTag      );
  velGrad_     = this->template create_field_request<FieldT>( velGradTag  );

  sign_ = 0.0;
  sign_ = positiveCurl_ ? 1.0 : -1.0;

  // grabs a -1 or 1 for checking whether we have inflow or outflow
  switch( info_.side ){
    case SpatialOps::PLUS_SIDE : sign_ *=  1.0; break;
    case SpatialOps::MINUS_SIDE: sign_ *= -1.0; break;
    default:
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "Invalid side in L345OffDiagonal Calc expression in NSCBC" << std::endl;
      throw std::runtime_error( msg.str() );
  }

  if( !doDiv_ ){
    divVel_   = this->template create_field_request<FieldT>( divVelTag   );
  }
}

//--------------------------------------------------------------------

template< typename FieldT, typename DivT >
void
L3L4L5OffDiagonal<FieldT,DivT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  divOp_ = opDB.retrieve_operator<DivT>();
}

//--------------------------------------------------------------------

template< typename FieldT, typename DivT >
void
L3L4L5OffDiagonal<FieldT,DivT>::
evaluate()
{
  using namespace SpatialOps;

  FieldT& result = this->value();

  const FieldT& vel      = vel_     ->field_ref();
  const FieldT& velGrad  = velGrad_ ->field_ref();

  if( doDiv_ ) masked_assign( mask_, result, sign_ * ( vel ) * ( (*divOp_)(velGrad) ) );
  else{
    const FieldT& divVel = divVel_->field_ref();
    masked_assign( mask_, result, sign_ * vel * divVel );
  }
}

//--------------------------------------------------------------------

#endif /* L3L4L5OffDiagonal_h */
