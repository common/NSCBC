/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *      \file   Lk.h
 *      \date   Created on: Mar 20, 2015
 *      \author Author: Derek Cline
 *
 *      \class Lk
 *      \brief Lk an expression for Navier-Stokes Characteristic Boundary
 *             Condition Calculations in the ODT code database
 *             See Sutherland and Kennedy 2003 or Coussement et al. 2012
 *             for formulations
 *             Corresponds to the species.
 *
 *      \tparam DivT this template parameter should be a one sided stencil
 *              corresponding to the direction of the boundary (plus or minus)
 *              NOTE: A plus face boundary requires a minus direction stencil
 *      \param velTag  velocity tag (normal to the boundary)
 *      \param specGrad species fraction tag
 *
 */

#ifndef Lk_h
#define Lk_h

#include <expression/ExprLib.h>

template< typename FieldT, typename DivT >
class Lk
  : public Expr::Expression<FieldT>
{
  DECLARE_FIELDS( FieldT, vel_, spec_,divSpec_ )

  const SpatialOps::SpatialMask<FieldT> mask_;
  const bool doDiv_;
  const DivT* divOp_;

  Lk( const SpatialOps::SpatialMask<FieldT>& mask,
      const Expr::Tag& velTag,
      const Expr::Tag& specTag,
      const Expr::Tag& divSpecTag );

  ~Lk(){}

public:

  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();

  class Builder : public Expr::ExpressionBuilder
  {
    const SpatialOps::SpatialMask<FieldT> mask_;
    const Expr::Tag velT_,specT_,divSpecT_;
  public:
    Builder( const Expr::Tag& result,
             const SpatialOps::SpatialMask<FieldT>& mask,
             const Expr::Tag& velTag,
             const Expr::Tag& specTag,
             const Expr::Tag& divSpecTag)
    : ExpressionBuilder(result),
      mask_    ( mask       ),
      velT_    ( velTag     ),
      specT_   ( specTag    ),
      divSpecT_( divSpecTag )
    {}

    Expr::ExpressionBase* build() const         {
      return new Lk<FieldT,DivT>(mask_,velT_,specT_,divSpecT_);
    }
  };

};



// ###################################################################
//
//                          Implementation
//
// ###################################################################
template< typename FieldT, typename DivT >
Lk<FieldT,DivT>::
Lk( const SpatialOps::SpatialMask<FieldT>& mask,
    const Expr::Tag& velTag,
    const Expr::Tag& specTag,
    const Expr::Tag& divSpecTag )
  : Expr::Expression<FieldT>(),
    mask_( mask ),
    doDiv_( divSpecTag == Expr::Tag() )
{
  this->set_gpu_runnable( true );

  vel_  = this->template create_field_request<FieldT>( velTag  );
  spec_ = this->template create_field_request<FieldT>( specTag );
  if( !doDiv_ ){
    divSpec_ = this->template create_field_request<FieldT>( divSpecTag   );
  }
}

//--------------------------------------------------------------------

template< typename FieldT, typename DivT >
void
Lk<FieldT,DivT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  divOp_ = opDB.retrieve_operator<DivT>();
}

//--------------------------------------------------------------------

template< typename FieldT, typename DivT >
void
Lk<FieldT,DivT>::
evaluate()
{
  using namespace SpatialOps;

  FieldT& result = this->value();

  const FieldT& spec = spec_->field_ref();
  const FieldT& vel  = vel_ ->field_ref();

  if( doDiv_ ) masked_assign( mask_, result, vel * (*divOp_)(spec) );
    else{
      const FieldT& divSpec = divSpec_->field_ref();
      masked_assign( mask_, result, vel * divSpec );
    }
}

//--------------------------------------------------------------------

#endif /* Lk_h */
