/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *      \file   LkSet.h
 *      \date   Created on: Mar 20, 2015
 *      \author Author: Derek Cline
 *
 *      \class LkSet
 *      \brief LkSet an expression for Navier-Stokes Characteristic Boundary
 *             Condition Calculations in the ODT code database
 *             See Sutherland and Kennedy 2003 or Coussement et al. 2012
 *             for formulations
 *             This corresponds to the characteristics of the species.
 *             We set this to zero for stability on an inflow and
 *             maintain the original Lk for an outflow.
 *
 *      \param LkTag Tag corresponding to the Lk Calculated Wave
 *      \param velTag Tag corresponding to the velocity
 *      \tparam DivT this template parameter should be a one sided stencil
 *              corresponding to the direction of the boundary (plus or minus)
 *              NOTE: A plus face boundary requires a minus direction stencil
 *
 */

#ifndef LkSet_h
#define LkSet_h

#include <expression/ExprLib.h>

template< typename FieldT >
class LkSet
  : public Expr::Expression<FieldT>
{
  DECLARE_FIELDS( FieldT, Lk_, vel_ )

  LkSet( const NSCBC::NSCBCInfo<FieldT>& info,
         const Expr::Tag& LkTag,
         const Expr::Tag& velTag  );

  ~LkSet(){}

public:
  void evaluate();

  class Builder : public Expr::ExpressionBuilder
  {
    const NSCBC::NSCBCInfo<FieldT> info_;
    const Expr::Tag LkT_, velT_;

  public:
    Builder( const Expr::Tag& result,
             const NSCBC::NSCBCInfo<FieldT>& info,
             const Expr::Tag& LkTag,
             const Expr::Tag& velTag )
    : ExpressionBuilder(result),
      info_( info   ),
      LkT_ ( LkTag  ),
      velT_( velTag )
    {}

    Expr::ExpressionBase* build() const{
      return new LkSet<FieldT>(info_,LkT_,velT_);
    }
  };

private:
  const NSCBC::NSCBCInfo<FieldT> info_;
  double side_;
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################
template< typename FieldT >
LkSet<FieldT>::
LkSet( const NSCBC::NSCBCInfo<FieldT>& info,
       const Expr::Tag& LkTag,
       const Expr::Tag& velTag )
  : Expr::Expression<FieldT>(),
    info_( info )
{
  this->set_gpu_runnable( true );

  Lk_  = this->template create_field_request<FieldT>( LkTag  );
  vel_ = this->template create_field_request<FieldT>( velTag );

  // grabs a -1 or 1 for checking whether we have inflow or outflow
  switch( info_.side ){
    case SpatialOps::PLUS_SIDE : side_ =  1.0; break;
    case SpatialOps::MINUS_SIDE: side_ = -1.0; break;
    default:
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "Invalid side in LKSet expression in NSCBC" << std::endl;
      throw std::runtime_error( msg.str() );
  }
}

//--------------------------------------------------------------------

template< typename FieldT >
void
LkSet<FieldT>::
evaluate()
{
  using namespace SpatialOps;
  FieldT& result = this->value();

  const FieldT& vel = vel_->field_ref();
  const FieldT& Lk  = Lk_ ->field_ref();

  //We maintain Lk for an outflow but set it for an inflow
  result <<= cond( (side_ * vel) > 0.0, Lk  )
                 (                      0.0 );
}

//--------------------------------------------------------------------

#endif /* LkSet_h */
