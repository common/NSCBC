/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *      \file   RPressure.h
 *      \date   Created on: Mar 20, 2015
 *      \author Author: Derek Cline
 *
 *      \class RPressure
 *      \brief RPressure an expression for Navier-Stokes Characteristic Boundary
 *             Condition Calculations.
 *             See Sutherland and Kennedy 2003 for formulation.
 *             This is the pressure source term utilized when setting
 *             the characteristic waves.
 *
 *      \param mask Mask for the boundary cells to be operated on
 *      \param temperatureTag Tag corresponding to the temperature
 *      \param cpTag Constant Pressure Heat Capacity
 *      \param cvTag Constant Volume Heat Capacity
 *      \param mixMolWeightTag The molecular weight of the mixture
 *      \param enthalpyTags Taglist corresponding to the enthalpies
 *      \param reactionTags Taglist corresponding to the species reaction terms
 *      \param specMW vector of the molecular weights of the species
 *      \param gasConstant Universal Gas Constant
 *      \tparam FieldT Volume Field Type
 *      \tparam DivT this template parameter should be a one sided stencil
 *              corresponding to the direction of the boundary (plus or minus)
 *              NOTE: A plus face boundary requires a minus direction stencil
 */

#ifndef RPressure_h
#define RPressure_h

#include <expression/ExprLib.h>

template< typename FieldT >
class RPressure : public Expr::Expression<FieldT>
{
  DECLARE_FIELDS( FieldT, temperature_, cp_, cv_, species_, mixMolWeight_ )

  DECLARE_VECTOR_OF_FIELDS( FieldT, enthalpies_ )
  DECLARE_VECTOR_OF_FIELDS( FieldT, reactions_  )

  const SpatialOps::SpatialMask<FieldT> mask_;
  const bool doSpecies_;
  const std::vector<double> specMW_;
  const double gasConstant_;
  const size_t nspec_;

  RPressure( const SpatialOps::SpatialMask<FieldT>& mask,
             const Expr::Tag& temperatureTag,
             const Expr::Tag& cpTag,
             const Expr::Tag& cvTag,
             const Expr::Tag& mixMolWeightTag,
             const Expr::TagList& enthalpyTags,
             const Expr::TagList& reactionTags,
             const std::vector<double>& specMW,
             const double gasConstant );

  RPressure( const SpatialOps::SpatialMask<FieldT>& mask );

  ~RPressure(){}

public:

  void evaluate();

  class Builder : public Expr::ExpressionBuilder
  {
    const SpatialOps::SpatialMask<FieldT> mask_;
    const Expr::Tag  temperatureT_, cpT_, cvT_, mixMolWeightT_;
    const Expr::TagList enthalpyT_, reactionT_;
    const bool doSpecies_;
    const std::vector<double> specMW_;
    const double gasConstant_;

  public:
    Builder( const Expr::Tag& result,
             const SpatialOps::SpatialMask<FieldT>& mask,
             const Expr::Tag& temperatureTag,
             const Expr::Tag& cpTag,
             const Expr::Tag& cvTag,
             const Expr::Tag& mixMolWeightTag,
             const Expr::TagList& enthalpyTags,
             const Expr::TagList& reactionTags,
             const std::vector<double>& specMW,
             const double gasConstant );

    Builder( const Expr::Tag& result,
             const SpatialOps::SpatialMask<FieldT>& mask );

    Expr::ExpressionBase* build() const;
  };

};



// ###################################################################
//
//                          Implementation
//
// ###################################################################

template< typename FieldT >
RPressure<FieldT>::
RPressure( const SpatialOps::SpatialMask<FieldT>& mask,
           const Expr::Tag& temperatureTag,
           const Expr::Tag& cpTag,
           const Expr::Tag& cvTag,
           const Expr::Tag& mixMolWeightTag,
           const Expr::TagList& enthalpyTags,
           const Expr::TagList& reactionTags,
           const std::vector<double>& specMW,
           const double gasConstant )
  : Expr::Expression<FieldT>(),
    mask_       ( mask        ),
    doSpecies_  ( true        ),
    specMW_     ( specMW      ),
    gasConstant_( gasConstant ),
    nspec_      ( enthalpyTags.size() )
{
  this->set_gpu_runnable( true );

  assert( reactionTags.size() == nspec_ );

  cp_           = this->template create_field_request<FieldT>( cpTag           );
  cv_           = this->template create_field_request<FieldT>( cvTag           );
  temperature_  = this->template create_field_request<FieldT>( temperatureTag  );
  mixMolWeight_ = this->template create_field_request<FieldT>( mixMolWeightTag );

  if( doSpecies_ ){
    this->template create_field_vector_request<FieldT>( enthalpyTags, enthalpies_ );
    this->template create_field_vector_request<FieldT>( reactionTags, reactions_ );
  }
}

//--------------------------------------------------------------------

template< typename FieldT >
RPressure<FieldT>::
RPressure( const SpatialOps::SpatialMask<FieldT>& mask )
  : Expr::Expression<FieldT>(),
    mask_       ( mask  ),
    doSpecies_  ( false ),
    gasConstant_( 0     ), //Should not be used
    nspec_      ( 0     )
{
  this->set_gpu_runnable( true );
}

//--------------------------------------------------------------------

template< typename FieldT >
void
RPressure<FieldT>::
evaluate()
{
  using namespace SpatialOps;

  FieldT& result = this->value();

  result <<= 0.0;
  if( doSpecies_ ){
    const FieldT& cp           = cp_          ->field_ref();
    const FieldT& cv           = cv_          ->field_ref();
    const FieldT& temperature  = temperature_ ->field_ref();
    const FieldT& mixMolWeight = mixMolWeight_->field_ref();

    //Gas Constant of the mixture
    SpatFldPtr<FieldT> mixGasConst = SpatialFieldStore::get<FieldT>( result );

    //Corresponds to the nth species
    SpatFldPtr<FieldT> xiN  = SpatialFieldStore::get<FieldT>( result );

    masked_assign( mask_, *xiN, enthalpies_[nspec_-1]->field_ref() - cp * temperature * mixMolWeight / specMW_[nspec_-1] );

    masked_assign( mask_, *mixGasConst, gasConstant_ / mixMolWeight );

    for( size_t i=0; i<nspec_-1; ++i ){
      masked_assign( mask_, result, result - ( *mixGasConst / cv ) * reactions_[i]->field_ref() * ( enthalpies_[i]->field_ref() - cp * temperature *  mixMolWeight  / specMW_[i] - *xiN) );
    }
  }
}

//--------------------------------------------------------------------

template< typename FieldT >
RPressure<FieldT>::Builder::
Builder( const Expr::Tag& result,
         const SpatialOps::SpatialMask<FieldT>& mask,
         const Expr::Tag& temperatureTag,
         const Expr::Tag& cpTag,
         const Expr::Tag& cvTag,
         const Expr::Tag& mixMolWeightTag,
         const Expr::TagList& enthalpyTags,
         const Expr::TagList& reactionTags,
         const std::vector<double>& specMW,
         const double gasConstant )
  : ExpressionBuilder( result ),
    mask_         ( mask            ),
    temperatureT_ ( temperatureTag  ),
    cpT_          ( cpTag           ),
    cvT_          ( cvTag           ),
    mixMolWeightT_( mixMolWeightTag ),
    enthalpyT_    ( enthalpyTags    ),
    reactionT_    ( reactionTags    ),
    doSpecies_    ( true            ),
    specMW_       ( specMW          ),
    gasConstant_  ( gasConstant     )
{}

//--------------------------------------------------------------------

template< typename FieldT >
RPressure<FieldT>::Builder::
Builder( const Expr::Tag& result,
         const SpatialOps::SpatialMask<FieldT>& mask )
  : ExpressionBuilder(result),
    mask_       ( mask  ),
    doSpecies_  ( false ),
    gasConstant_( 0 ) //Shouldn't be used
{}

//--------------------------------------------------------------------

template< typename FieldT >
Expr::ExpressionBase*
RPressure<FieldT>::Builder::build() const
{
  if( doSpecies_ ) return new RPressure<FieldT>(mask_,temperatureT_,cpT_,cvT_,mixMolWeightT_,enthalpyT_,reactionT_,specMW_,gasConstant_ );
  else             return new RPressure<FieldT>(mask_);
}

//--------------------------------------------------------------------

#endif /* RPressure_h */
