/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *      \file   NSCBCToolsAndDefs.h
 *
 *      \struct NSCBCInfo
 *      \brief Contains information about a particular boundary.
 *
 *      \param mask SpatialOps Mask that covers points directly interior to a boundary
 *                  (i.e. NOT ghost cells exterior to the domain).
 *      \param side SpatialOps Plus or Minus side.  Minus is corresponds with the first
 *                  cell in the domain and Plus corresponds to the last.
 *      \param dir  Direction normal to the boundary.  I.e. X, Y or Z
 *      \param type Type of flow.  If we wish to specify a hard inflow
 *      \param farfieldpressure Relaxation pressure.  Typically, 1 atm for an open domain
 *      \param domainlength     Double that is the length of the domain normal to the boundary.
 *                              For example, a 1 cm by 2 cm domain will have a domain length of
 *                              1 for the x-direction and 2 for the y-direction.  Also associated
 *                              with the relaxation terms in the pressure wave amplitudes
 *      \tparam FieldT Volume Field Type
 */

#ifndef NSCBCTOOLSANDDEFS_H_
#define NSCBCTOOLSANDDEFS_H_

#include <string>
#include <vector>

#include <spatialops/SpatialOpsDefs.h>
#include <spatialops/structured/IndexTriplet.h>
#include <spatialops/structured/SpatialMask.h>
#include <spatialops/structured/stencil/OneSidedOperatorTypes.h>

#include <expression/Tag.h>

namespace NSCBC{

  typedef SpatialOps::UnitTriplet<SpatialOps::XDIR>::type  XPlus;
  typedef SpatialOps::UnitTriplet<SpatialOps::YDIR>::type  YPlus;
  typedef SpatialOps::UnitTriplet<SpatialOps::ZDIR>::type  ZPlus;
  typedef XPlus::Negate XMinus;
  typedef YPlus::Negate YMinus;
  typedef ZPlus::Negate ZMinus;

  typedef SpatialOps::OneSidedStencil3<XPlus > XPDivT;
  typedef SpatialOps::OneSidedStencil3<XMinus> XMDivT;
  typedef SpatialOps::OneSidedStencil3<YPlus > YPDivT;
  typedef SpatialOps::OneSidedStencil3<YMinus> YMDivT;
  typedef SpatialOps::OneSidedStencil3<ZPlus > ZPDivT;
  typedef SpatialOps::OneSidedStencil3<ZMinus> ZMDivT;

  enum TransportVal{
    DENSITY,
    MOMENTUM_X,
    MOMENTUM_Y,
    MOMENTUM_Z,
    ENERGY,
    SPECIES
  };

  enum Direction
  {
    XDIR  = SpatialOps::XDIR ::value,   ///< The x-direction
    YDIR  = SpatialOps::YDIR ::value,   ///< The y-direction
    ZDIR  = SpatialOps::ZDIR ::value,   ///< The z-direction
    NODIR = SpatialOps::NODIR::value    ///< an invalid direction
  };

  enum BCType
  {
    NONREFLECTING,
    HARD_INFLOW,
    WALL,
    NO_TYPE
  };


  template< typename FieldT >
  struct NSCBCInfo{
    NSCBCInfo( const typename SpatialOps::SpatialMask<FieldT>& mask,
               const SpatialOps::BCSide side,
               const Direction dir,
               const BCType type,
               const std::string jobName = "",
               const double farfieldpressure = 0.0,
               const double domainlength = 0.0 )
    : side( side ),
      direction( dir ),
      type( type ),
      jobName( jobName),
      mask( mask ),
      farfieldpressure( farfieldpressure ),
      domainlength( domainlength )
    {}
    const SpatialOps::BCSide side;
    Direction direction;
    BCType type;
    std::string jobName; //only necessary if multiple instances need to be built
    int masknumber;
    SpatialOps::SpatialMask<FieldT> mask;
    double farfieldpressure; // Necessary for L1Outflow
    double domainlength;     // Necessary for L1Outflow
  };

  //---------------------------------------------------------------------
  //Returns string names for use in creating tags
  inline std::string face_string( const Direction dir )
  {
    switch( dir ){
      case XDIR: return "_XFace";
      case YDIR: return "_YFace";
      case ZDIR: return "_ZFace";
      default:   return "INVALID_DIR";
    }
  }

  inline std::string side_string( const SpatialOps::BCSide& side )
  {
    switch( side ){
      case SpatialOps::MINUS_SIDE: return "_Minus";
      case SpatialOps::PLUS_SIDE:  return "_Plus";
      default:                     return "INVALID_SIDE";
    }
  }

  inline std::string type_string( const BCType type )
  {
    switch( type ){
      case NONREFLECTING: return "_NonreflectingFlow";
      case HARD_INFLOW:   return "_HardInflow";
      case WALL:          return "_Wall";
      default:            return "_Unknown_Type";
    }
  }

  //---------------------------------------------------------------------

  //---------------------------------------------------------------------
  //Returns defs associated with strings for use in parsing input files
  inline Direction face_def( const std::string string )
  {
    if     ( string == "XFace") return XDIR;
    else if( string == "YFace") return YDIR;
    else if( string == "ZFace") return ZDIR;
    else                        return NODIR;
  }

  inline SpatialOps::BCSide side_def( const std::string string )
  {
    if     ( string == "MinusSide") return SpatialOps::MINUS_SIDE;
    else if( string == "PlusSide")  return SpatialOps::PLUS_SIDE;
    else                            return SpatialOps::NO_SIDE;
  }

  inline BCType type_def( const std::string string )
  {
    if     ( string == "NonreflectingFlow")          return NONREFLECTING;
    else if( string == "HardInflow")                 return HARD_INFLOW;
    else if( string == "Wall")                       return WALL;
    else                                             return NO_TYPE;
  }

  //---------------------------------------------------------------------

}//end namespace

#endif /* NSCBCTOOLSANDDEFS_H_ */
