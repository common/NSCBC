/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *      \file   SpeedOfSound.h
 *      \date   Created on: April 27, 2015
 *      \author Author: Derek Cline
 *
 *      \class SpeedOfSound
 *      \brief An expression that calculates the speed of sound of the system
 *
 *      \param info Struct containing various NSCBC info (side, type, etc).
 *      \param pressureTag Tag corresponding to the pressure
 *      \param densityTag Tag corresponding to the density
 *      \param cpTag Tag corresponding to the constant pressure specific heat capacity
 *      \param cvTag Tag corresponding to the constant volume specific heat capacity
 *      \tparam FieldT Volume Field Type
 *
 */

#ifndef SpeedOfSound_h
#define SpeedOfSound_h

#include <expression/ExprLib.h>

#include <cmath>

namespace NSCBC{

  template< typename FieldT >
  class SpeedOfSound : public Expr::Expression<FieldT>
  {
    DECLARE_FIELDS( FieldT, pressure_, density_, cp_, cv_ )

    SpeedOfSound( const Expr::Tag& pressureTag,
                  const Expr::Tag& densityTag,
                  const Expr::Tag& cpTag,
                  const Expr::Tag& cvTag );

    ~SpeedOfSound(){}

  public:

    void evaluate();

    class Builder : public Expr::ExpressionBuilder
    {
      const Expr::Tag pressureT_,densityT_,cpT_,cvT_;
    public:
      Builder( const Expr::Tag& result,
               const Expr::Tag& pressureTag,
               const Expr::Tag& densityTag,
               const Expr::Tag& cpTag,
               const Expr::Tag& cvTag )
        : ExpressionBuilder(result),
          pressureT_( pressureTag ),
          densityT_ ( densityTag  ),
          cpT_      ( cpTag       ),
          cvT_      ( cvTag       )
      {}

      Expr::ExpressionBase* build() const{
        return new SpeedOfSound<FieldT>(pressureT_,densityT_,cpT_,cvT_);
      }

    };
  };



  // ###################################################################
  //
  //                          Implementation
  //
  // ###################################################################

  template< typename FieldT >
  SpeedOfSound<FieldT>::
  SpeedOfSound( const Expr::Tag& pressureTag,
                const Expr::Tag& densityTag,
                const Expr::Tag& cpTag,
                const Expr::Tag& cvTag )
    : Expr::Expression<FieldT>()
  {
    this->set_gpu_runnable( true );

    pressure_ = this->template create_field_request<FieldT>( pressureTag );
    density_  = this->template create_field_request<FieldT>( densityTag  );
    cp_       = this->template create_field_request<FieldT>( cpTag       );
    cv_       = this->template create_field_request<FieldT>( cvTag       );
  }

  //--------------------------------------------------------------------

  template< typename FieldT >
  void
  SpeedOfSound<FieldT>::
  evaluate()
  {
    using namespace SpatialOps;

    FieldT& result = this->value();

    const FieldT& cp       = cp_      ->field_ref();
    const FieldT& cv       = cv_      ->field_ref();
    const FieldT& pressure = pressure_->field_ref();
    const FieldT& density  = density_ ->field_ref();

    result <<= sqrt( (cp / cv) * (pressure / density) );
  }

  //--------------------------------------------------------------------

}//end namespace

#endif /* SpeedOfSound_h */
