/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *      \file   TagManager.h
 *      \date   Created on: Feb 24, 2015
 *      \author Author: Nathan Yonkee
 *
 */

#ifndef NSCBC_TAGMANAGER_H_
#define NSCBC_TAGMANAGER_H_

#include <expression/Tag.h>
#include <iostream>
#include <sstream>
#include <exception>
#include <stdexcept>

namespace NSCBC{

  enum TagName{
    U,
    V,
    W,
    FACEVEL,
    DVEL,
    DUVEL,
    DVVEL,
    DWVEL,
    DP,
    DDENS,
    T,
    P,
    DVELHARDINFLOW,
    DPHARDINFLOW,
    RHO,
    MMW,
    CP,
    CV,
    C,
    E0,
    L1,
    L2,
    L345D,
    L345O1,
    L345O2,
    L1_M,
    L2_M,
    L345D_M,
    L345O1_M,
    L345O2_M,
    LK_M,
    DF_DENSITY_ADD,
    DF_DENSITY_SUB,
    DF_MOMENTUM_X_ADD,
    DF_MOMENTUM_X_SUB,
    DF_MOMENTUM_Y_ADD,
    DF_MOMENTUM_Y_SUB,
    DF_MOMENTUM_Z_ADD,
    DF_MOMENTUM_Z_SUB,
    DF_ENERGY_ADD,
    DF_ENERGY_SUB,
    LK,
    RP
  };

  enum TagListName{
    H_N,
    R_N,
    Y_N,
    DIVY_N,
    LKS,
    LKS_M,
    DFn_S_ADD,
    DFn_S_SUB
  };

  /**
   *  \class TagManager
   *  \brief Contains enumerated TagNames associated with Expr::Tags to manage
   *         all tags necessary for applying NSCBC
   *
   *  \param tags      TagMap of each individual tag
   *  \param tagLists  TagMapList of groups of tags (e.g.
   *                   species tags, reaction rate tags, etc)
   *  \param doSpecies Boolean to turn reaction rates on and off.
   *
   *  \par Example Setup without species
   *  \code{.cpp}
   *   std::map< NSCBC::TagName,     Expr::Tag     > tags;
   *
   *   tags[NSCBC::U] = Expr::Tag( 'x_velocity', STATE_NONE);
   *   //Skipping the rest of the necessary tags for brevity
   *   NSCBC::TagManager tagManager(tags, false);
   *  \endcode
   *
   *  \par Example Setup with species
   *  \code{.cpp}
   *   std::map< NSCBC::TagName,     Expr::Tag     > tags;
   *   std::map< NSCBC::TagListName, Expr::TagList > tagLists;
   *
   *   tags[NSCBC::U] = Expr::Tag( 'x_velocity', STATE_NONE);
   *   tagLists[NSCBC::Y_N] = lbmsOptions.pokittOptions.species_tags( bundle->dir(), STATE_NONE );
   *   //Skipping the rest of the necessary tags for brevity
   *   NSCBC::TagManager tagManager(tags, tagLists, true);
   *  \endcode
   *
   *  Note that the face velocity is the velocity normal to the boundary that
   *  the NSCBC are being applied to.  Additionally, unused tags do not need to
   *  be initialized (e.g. in an 1D X-direction case, we do not need v, or w).
   *
   */
class TagManager{

    typedef std::map< TagName, Expr::Tag > TagMap;
    typedef std::map< TagListName, Expr::TagList > TagListMap;

    TagMap tags_;
    TagListMap lists_;

public:

    bool doSpecies_;

  TagManager( ) : tags_( TagMap() ), lists_( TagListMap() ), doSpecies_( false )  {};

  TagManager( const std::map< TagName, Expr::Tag >& tags, bool doSpecies )
  : tags_( tags ), lists_( TagListMap() ), doSpecies_( doSpecies )  {};

  TagManager( const std::map< TagName, Expr::Tag >& tags,
              const std::map< TagListName, Expr::TagList >& tagLists, bool doSpecies )
  : tags_( tags ), lists_( tagLists ), doSpecies_( doSpecies )
  { }

  TagManager( const std::vector< std::pair< TagName, Expr::Tag > >& tags,
              const std::vector< std::pair< TagListName, Expr::TagList > >& tagLists )
  : doSpecies_( true )
  {
    std::vector< std::pair< TagName, Expr::Tag > >::const_iterator iT = tags.begin();
    std::vector< std::pair< TagListName, Expr::TagList > >::const_iterator iL = tagLists.begin();
    for( ; iT != tags.end(); ++iT )
      add_tag( *iT );
    for( ; iL != tagLists.end(); ++iL )
      add_taglist( *iL );
  }

  void add_tag( std::pair< TagName,     Expr::Tag     > tag )
  { tags_.insert( tag ); }
  void add_tag( TagName name, Expr::Tag tag )
  { tags_.insert( std::make_pair( name, tag ) ); }

  void add_taglist( std::pair< TagListName, Expr::TagList > tagList ){
    lists_.insert( tagList );
  }

  const Expr::Tag& operator[](TagName name){
    if( tags_.find( name ) != tags_.end() )
      return tags_.find( name )->second;
    else{
      add_tag( std::make_pair( name, Expr::Tag() ) );
      return tags_.find( name )->second;
    }
  }

  const Expr::TagList& operator[](TagListName name){
    if( lists_.find( name ) != lists_.end() )
      return lists_.find( name )->second;
    else{
      Expr::TagList tempList; Expr::Tag emptyTag;
      for( size_t i=0; i != (lists_.find( DFn_S_ADD )->second).size(); ++i ){
        tempList = tag_list(emptyTag, tempList);
      }
      add_taglist( std::make_pair( name, tempList ) );
      return lists_.find( name )->second;
    }
  }

  TagMap all_tags() const {
    return tags_;
  }

  TagListMap all_tagLists() const {
    return lists_;
  }

  void append_managers( const TagManager& other )
  {
    tags_.insert( other.all_tags().begin(), other.all_tags().end() );
    lists_.insert( other.all_tagLists().begin(), other.all_tagLists().end() );
  }

};

}
#endif /* NSCBC_TAGMANAGER_H_ */
