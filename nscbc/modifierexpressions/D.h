/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/*
 * D.h
 * Created on:
 * Author: Nathan Yonkee
 *
 */

#ifndef NSCBCdFromL_h
#define NSCBCdFromL_h

#include <spatialops/Nebo.h>

#include <nscbc/NSCBCToolsAndDefs.h>

using SpatialOps::PLUS_SIDE;
using SpatialOps::MINUS_SIDE;
using SpatialOps::masked_assign;

template< typename FieldT >
inline void nscbc_d1( FieldT& d1,
                      const SpatialOps::SpatialMask< FieldT >& mask,
                      const double side,
                      const FieldT& l1,
                      const FieldT& l2,
                      const FieldT& l345,
                      const FieldT& c )
{
  masked_assign( mask, d1, ( l1 + l2 ) / ( c * c ) + side * l345  );
}

//--------------------------------------------------------------------

//Only d1 is necessary as only density is set
template< typename FieldT >
inline void nscbc_d1_hardinflow( FieldT& d1,
                                 const SpatialOps::SpatialMask< FieldT >& mask,
                                 const FieldT& l1,
                                 const FieldT& l2,
                                 const FieldT& rho,
                                 const FieldT& pressure,
                                 const FieldT& Rp,
                                 const FieldT& Rmix,
                                 const FieldT& cv,
                                 const FieldT& c )
{
  masked_assign( mask, d1, ( 1 + Rmix/cv ) * ( 1 / ( c * c ) ) * ( l1 + l2 ) - (rho / pressure ) * Rp );
  //possible alternative
//    masked_assign( mask, d1, (rho / pressure ) * ( l1 + l2 - Rp ) );
}

//--------------------------------------------------------------------

template< typename FieldT >
inline void nscbc_d234diag( FieldT& d234,
                            const typename SpatialOps::SpatialMask< FieldT >& mask,
                            const double side,
                            const FieldT& l1,
                            const FieldT& l2,
                            const FieldT& c,
                            const FieldT& rho )
{
  masked_assign( mask, d234, side * ( l2 - l1 ) / ( c * rho ) );
}

//--------------------------------------------------------------------

template< typename FieldT >
inline void nscbc_d234offdiag( FieldT& d234,
                               const SpatialOps::SpatialMask< FieldT >& mask,
                               const double side,
                               const double curl,
                               const FieldT& l345 )
{
  //Curl is the right hand rule from the d direction onto the boundary
  //d2 on a  Y-boundary has a positive curl, Z-boundary has a negative curl
  //d3 on an X-boundary has a negative curl, Z-boundary has a positive curl
  //d4 on an X-boundary has a positive curl, Z-boundary has a negative curl
  masked_assign( mask, d234, -1.0 * side * curl * l345 );
}

//--------------------------------------------------------------------

template< typename FieldT >
inline void nscbc_d5( FieldT& d5,
                      const SpatialOps::SpatialMask< FieldT >& mask,
                      const FieldT& l1,
                      const FieldT& l2 )
{
  masked_assign( mask, d5, l1 + l2 );
}

//--------------------------------------------------------------------

template< typename FieldT >
inline void nscbc_d5pn( FieldT& d5pn,
                        const SpatialOps::SpatialMask< FieldT >& mask,
                        const FieldT& l5pn )
{
  //dac is this even necessary?  Keeping it in case we need it for transverse terms
  masked_assign( mask, d5pn, l5pn );
}

#endif
