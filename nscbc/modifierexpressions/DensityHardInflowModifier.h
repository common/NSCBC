/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *      \file   DensityHardInflowModifier.h
 *      \date   Created on: Mar 20, 2015
 *      \author Author: Derek Cline
 *
 *      \class DensityHardInflowModifier
 *      \brief DensityHardInflowModifier
 *
 *      \param mask Spatial Mask associated with the boundary cells
 *      \param side SpatialOps side (plus or minus)
 *      \param cTag Speed of Sound
 *      \param l1Tag First wave amplitude (calculated from set primitives)
 *      \param l2Tag Second wave amplitude (calculated from set primitives)
 *      \param rhoTag density tag
 *      \param pressureTag pressure tag
 *      \param RpTag Pressure source term tag
 *      \param RmixTag Gas Gas constant of the mixture
 *      \param cvTag volume heat capacity
 *      \param cTag speed of sound
 *      \tparam FieldT Volume Field Type
 *
 */
#ifndef DensityHardInflowModifier_h
#define DensityHardInflowModifier_h

#include <expression/ExprLib.h>
#include "D.h"

template< typename FieldT >
class DensityHardInflowModifier : public Expr::Expression<FieldT>
{
public:

  class Builder : public Expr::ExpressionBuilder
  {
    const SpatialOps::SpatialMask<FieldT> mask_;
    const Expr::Tag l1T_, l2T_, rhoT_, pressureT_, RpT_, RmixT_, cvT_, cT_;
  public:
    Builder( const Expr::Tag& result,
             const SpatialOps::SpatialMask<FieldT>& mask,
             const Expr::Tag& l1Tag,
             const Expr::Tag& l2Tag,
             const Expr::Tag& rhoTag,
             const Expr::Tag& pressureTag,
             const Expr::Tag& RpTag,
             const Expr::Tag& RmixTag,
             const Expr::Tag& cvTag,
             const Expr::Tag& cTag )
    : ExpressionBuilder(result),
      mask_     ( mask        ),
      l1T_      ( l1Tag       ),
      l2T_      ( l2Tag       ),
      rhoT_     ( rhoTag      ),
      pressureT_( pressureTag ),
      RpT_      ( RpTag       ),
      RmixT_    ( RmixTag     ),
      cvT_      ( cvTag       ),
      cT_       ( cTag        )
    {}

    Expr::ExpressionBase* build() const{
      return new DensityHardInflowModifier<FieldT>(mask_,l1T_,l2T_,rhoT_,pressureT_,RpT_,RmixT_,cvT_,cT_);
    }
  };

  void evaluate();

private:

  DensityHardInflowModifier( const SpatialOps::SpatialMask<FieldT>& mask,
                             const Expr::Tag& l1Tag,
                             const Expr::Tag& l2Tag,
                             const Expr::Tag& rhoTag,
                             const Expr::Tag& pressureTag,
                             const Expr::Tag& RpTag,
                             const Expr::Tag& RmixTag,
                             const Expr::Tag& cvTag,
                             const Expr::Tag& cTag );

  ~DensityHardInflowModifier(){}

  DECLARE_FIELDS( FieldT, l1_, l2_, rho_, pressure_, Rp_, Rmix_, cv_, c_ )

  const SpatialOps::SpatialMask<FieldT> mask_;
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################
template< typename FieldT >
DensityHardInflowModifier<FieldT>::
DensityHardInflowModifier( const SpatialOps::SpatialMask<FieldT>& mask,
                           const Expr::Tag& l1Tag,
                           const Expr::Tag& l2Tag,
                           const Expr::Tag& rhoTag,
                           const Expr::Tag& pressureTag,
                           const Expr::Tag& RpTag,
                           const Expr::Tag& RmixTag,
                           const Expr::Tag& cvTag,
                           const Expr::Tag& cTag )
  : Expr::Expression<FieldT>(),
    mask_  ( mask   )
{
  this->set_gpu_runnable( true );

  l1_       = this->template create_field_request<FieldT>( l1Tag       );
  l2_       = this->template create_field_request<FieldT>( l2Tag       );
  rho_      = this->template create_field_request<FieldT>( rhoTag      );
  pressure_ = this->template create_field_request<FieldT>( pressureTag );
  Rp_       = this->template create_field_request<FieldT>( RpTag       );
  Rmix_     = this->template create_field_request<FieldT>( RmixTag     );
  cv_       = this->template create_field_request<FieldT>( cvTag       );
  c_        = this->template create_field_request<FieldT>( cTag        );
}

//--------------------------------------------------------------------

template< typename FieldT >
void
DensityHardInflowModifier<FieldT>::
evaluate()
{
  using namespace SpatialOps;

  FieldT& result = this->value();

  const FieldT& l1       = l1_      ->field_ref();
  const FieldT& l2       = l2_      ->field_ref();
  const FieldT& rho      = rho_     ->field_ref();
  const FieldT& pressure = pressure_->field_ref();
  const FieldT& Rp       = Rp_      ->field_ref();
  const FieldT& Rmix     = Rmix_    ->field_ref();
  const FieldT& cv       = cv_      ->field_ref();
  const FieldT& c        = c_       ->field_ref();

  SpatFldPtr<FieldT> d1 = SpatialFieldStore::get<FieldT>( result );

  nscbc_d1_hardinflow( *d1, mask_, l1, l2, rho, pressure, Rp, Rmix, cv, c );

  masked_assign( mask_, result, -(*d1) );
}

//--------------------------------------------------------------------

#endif /* DensityHardInflowModifier_h */
