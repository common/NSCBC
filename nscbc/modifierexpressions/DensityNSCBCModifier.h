/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *      \file   DensityNSCBCModifier.h
 *      \date   Created on: Mar 20, 2015
 *      \author Author: Derek Cline
 *
 *      \class DensityNSCBCModifier
 *      \brief DensityNSCBCModifier an expression for Navier-Stokes Characteristic
 *             Boundary Condition Calculations in the ODT code database.  See
 *             Sutherland and Kennedy 2003 or Coussement et al. 2012 for formulations
 *
 *             This modifier will be attached as a modifier expression of the RHS
 *             of the Density (continuity) Transport Equation in the Navier-Stokes Eqns
 *             twice, first to subtract the calculated wave amplitudes and then to
 *             add back in the set wave amplitudes
 *
 *      \param mask Spatial Mask associated with the boundary cells
 *      \param side SpatialOps side (plus or minus)
 *      \param cTag Speed of Sound
 *      \param l1Tag First wave amplitude (set for addition, calculated for subtraction)
 *      \param l2Tag Second wave amplitude (set for addition, calculated for subtraction)
 *      \param l345Tag Diagonal wave amplitude (L3 = X, L4 = Y, L5 = Z boundary).
 *      \tparam FieldT Volume Field Type
 *
 */
#ifndef DensityNSCBCModifier_h
#define DensityNSCBCModifier_h

#include <expression/ExprLib.h>
#include "D.h"

template< typename FieldT >
class DensityNSCBCModifier : public Expr::Expression<FieldT>
{
public:

  class Builder : public Expr::ExpressionBuilder
  {
    const SpatialOps::SpatialMask<FieldT> mask_;
    const double side_;
    const Expr::Tag cT_, l1T_, l2T_, l345T_;
    const double addsub_;
  public:
    Builder( const Expr::Tag& result,
             const SpatialOps::SpatialMask<FieldT>& mask,
             const double side,
             const Expr::Tag& cTag,
             const Expr::Tag& l1Tag,
             const Expr::Tag& l2Tag,
             const Expr::Tag& l345Tag,
             const double addsub )
    : ExpressionBuilder(result),
      mask_ ( mask    ),
      side_ ( side    ),
      cT_   ( cTag    ),
      l1T_  ( l1Tag   ),
      l2T_  ( l2Tag   ),
      l345T_( l345Tag ),
      addsub_( addsub )
    {}

    Expr::ExpressionBase* build() const{
      return new DensityNSCBCModifier<FieldT>(mask_,side_,cT_,l1T_,l2T_,l345T_, addsub_ );
    }
  };

  void evaluate();

private:

  DensityNSCBCModifier( const SpatialOps::SpatialMask<FieldT>& mask,
                        const double side,
                        const Expr::Tag& cTag,
                        const Expr::Tag& l1Tag,
                        const Expr::Tag& l2Tag,
                        const Expr::Tag& l345Tag,
                        const double addsub);

  ~DensityNSCBCModifier(){}

  DECLARE_FIELDS( FieldT, c_, l1_, l2_, l345_ )

  const SpatialOps::SpatialMask<FieldT> mask_;
  const double side_;
  const double addsub_;
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################
template< typename FieldT >
DensityNSCBCModifier<FieldT>::
DensityNSCBCModifier( const SpatialOps::SpatialMask<FieldT>& mask,
                      const double side,
                      const Expr::Tag& cTag,
                      const Expr::Tag& l1Tag,
                      const Expr::Tag& l2Tag,
                      const Expr::Tag& l345Tag,
                      const double addsub)
  : Expr::Expression<FieldT>(),
    mask_  ( mask   ),
    side_  ( side   ),
    addsub_( addsub )
{
  this->set_gpu_runnable( true );

  c_    = this->template create_field_request<FieldT>( cTag    );
  l1_   = this->template create_field_request<FieldT>( l1Tag   );
  l2_   = this->template create_field_request<FieldT>( l2Tag   );
  l345_ = this->template create_field_request<FieldT>( l345Tag );
}

//--------------------------------------------------------------------

template< typename FieldT >
void
DensityNSCBCModifier<FieldT>::
evaluate()
{
  using namespace SpatialOps;

  FieldT& result = this->value();

  const FieldT& c    = c_   ->field_ref();
  const FieldT& l1   = l1_  ->field_ref();
  const FieldT& l2   = l2_  ->field_ref();
  const FieldT& l345 = l345_->field_ref();

  SpatFldPtr<FieldT> d1 = SpatialFieldStore::get<FieldT>( result );

  nscbc_d1( *d1, mask_, side_, l1, l2, l345, c );
  masked_assign( mask_, result, result - (addsub_)*(*d1) );
}

//--------------------------------------------------------------------

#endif /* DensityNSCBCModifier_h */
