/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *      \file   EnergyNSCBCModifier.h
 *      \date   Created on: Mar 20, 2015
 *      \author Author: Derek Cline
 *
 *      \class EnergyNSCBCModifier
 *      \brief EnergyNSCBCModifier an expression for Navier-Stokes Characteristic
 *             Boundary Condition Calculations in the ODT code database.  See
 *             Sutherland and Kennedy 2003 or Coussement et al. 2012 for formulations
 *
 *             This modifier will be attached as a modifier expression of the RHS
 *             of the Energy Transport Equation in the Navier-Stokes Eqns
 *             twice, first to subtract the calculated wave amplitudes and then to
 *             add back in the set wave amplitudes
 *
 *      \param mask Spatial Mask associated with the boundary cells
 *      \param side SpatialOps side (plus or minus)
 *      \param eTag Total internal Energy
 *      \param cTag Speed of Sound
 *      \param densityTag Density
 *      \param velDTag Velocity of the diagonal term (normal to the boundary)
 *      \param velO1Tag First perpendiular velocity ( e.g. x-boundary => y velocity
 *                                                    y,z-boundary => x-velocity    )
 *      \param velO2Tag Second perpendiular velocity ( e.g. x,y-boundary => z velocity
 *                                                     z-boundary => y-velocity        )
 *      \param temperatureTag Temperature
 *      \param cpTag Constant Pressure Heat Capacity
 *      \param cvTag Constant Volume Heat Capacity
 *      \param mixMolWeightTag Molecular Weight of the Mixture
 *      \param enthalpyTags Taglist of the enthalpy terms
 *      \param l1Tag First wave amplitude (set for addition, calculated for subtraction)
 *      \param l2Tag Second wave amplitude (set for addition, calculated for subtraction)
 *      \param l345DTag Diagonal wave amplitude (L3 = X, L4 = Y, L5 = Z boundary).
 *      \param l345O1Tag Off Diagonal wave amplitude.  Note that O1 corresponds to the SECOND
 *                       perpendicular direction to the boundary.
 *      \param l345O2Tag Off Diagonal wave amplitude.  Note that O2 corresponds to the FIRST
 *                       perpendicular direction to the boundary.
 *      \param lpkTags Species L tags.
 *      \param specMW Vector of molecular weights corresponding to the species
 *      \param gasConstant Universal Gas Constant
 *      \param doSpecies Boolean, for if we have reactions
 *      \tparam FieldT Volume Field Type
 *
 */

#ifndef EnergyNSCBCModifier_h
#define EnergyNSCBCModifier_h

#include <expression/ExprLib.h>
#include "D.h"

template< typename FieldT >
class EnergyNSCBCModifier : public Expr::Expression<FieldT>
{
public:
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    // All three directions
    Builder( const Expr::Tag& result,
             const NSCBC::NSCBCInfo<FieldT>& info,
             const double side,
             const Expr::Tag& eTag,
             const Expr::Tag& cTag,
             const Expr::Tag& densityTag,
             const Expr::Tag& velDTag,
             const Expr::Tag& velO1Tag,
             const Expr::Tag& velO2Tag,
             const Expr::Tag& temperatureTag,
             const Expr::Tag& cpTag,
             const Expr::Tag& cvTag,
             const Expr::Tag& mixMolWeightTag,
             const Expr::TagList& enthalpyTags,
             const Expr::Tag& l1Tag,
             const Expr::Tag& l2Tag,
             const Expr::Tag& l345DTag,
             const Expr::Tag& l345O1Tag,
             const Expr::Tag& l345O2Tag,
             const Expr::TagList& lpkTags,
             const std::vector<double>& specMW,
             const double gasConstant,
             const bool doSpecies,
             const double addsub );

    //Two directions
    Builder( const Expr::Tag& result,
             const NSCBC::NSCBCInfo<FieldT>& info,
             const double side,
             const Expr::Tag& eTag,
             const Expr::Tag& cTag,
             const Expr::Tag& densityTag,
             const Expr::Tag& velDTag,
             const Expr::Tag& velO1Tag,
             const Expr::Tag& temperatureTag,
             const Expr::Tag& cpTag,
             const Expr::Tag& cvTag,
             const Expr::Tag& mixMolWeightTag,
             const Expr::TagList& enthalpyTags,
             const Expr::Tag& l1Tag,
             const Expr::Tag& l2Tag,
             const Expr::Tag& l345DTag,
             const Expr::Tag& l345O2Tag,
             const Expr::TagList& lpkTags,
             const std::vector<double>& specMW,
             const double gasConstant,
             const bool doSpecies,
             const double addsub );

    //One direction
    Builder( const Expr::Tag& result,
             const NSCBC::NSCBCInfo<FieldT>& info,
             const double side,
             const Expr::Tag& eTag,
             const Expr::Tag& cTag,
             const Expr::Tag& densityTag,
             const Expr::Tag& velDTag,
             const Expr::Tag& temperatureTag,
             const Expr::Tag& cpTag,
             const Expr::Tag& cvTag,
             const Expr::Tag& mixMolWeightTag,
             const Expr::TagList& enthalpyTags,
             const Expr::Tag& l1Tag,
             const Expr::Tag& l2Tag,
             const Expr::Tag& l345DTag,
             const Expr::TagList& lpkTags,
             const std::vector<double>& specMW,
             const double gasConstant,
             const bool doSpecies,
             const double addsub );

    Expr::ExpressionBase* build() const;

  private:
    const NSCBC::NSCBCInfo<FieldT> info_;
    const double side_;
    const Expr::Tag eT_, cT_, densityT_, velDT_, velO1T_, velO2T_, temperatureT_, cpT_, cvT_, mixMolWeightT_;
    const Expr::TagList enthalpyT_;
    const Expr::Tag l1T_, l2T_, l345DT_, l345O1T_, l345O2T_;
    const Expr::TagList lpkT_;
    const std::vector<double> specMW_;
    const double gasConstant_;
    const bool doSpecies_;
    const double addsub_;
  };

  void evaluate();

private:

  EnergyNSCBCModifier( const NSCBC::NSCBCInfo<FieldT>& info,
                       const double side,
                       const Expr::Tag& eTag,
                       const Expr::Tag& cTag,
                       const Expr::Tag& densityTag,
                       const Expr::Tag& velDTag,
                       const Expr::Tag& velO1Tag,
                       const Expr::Tag& velO2Tag,
                       const Expr::Tag& temperatureTag,
                       const Expr::Tag& cpTag,
                       const Expr::Tag& cvTag,
                       const Expr::Tag& mixMolWeightTag,
                       const Expr::TagList& enthalpyTags,
                       const Expr::Tag& l1Tag,
                       const Expr::Tag& l2Tag,
                       const Expr::Tag& l345DTag,
                       const Expr::Tag& l345O1Tag,
                       const Expr::Tag& l345O2Tag,
                       const Expr::TagList& lpkTags,
                       const std::vector<double>& specMW,
                       const double gasConstant,
                       const bool doSpecies,
                       const double addsub );

  ~EnergyNSCBCModifier(){}

  DECLARE_FIELDS( FieldT, c_, e_, density_, velD_, velO1_, velO2_ )
  DECLARE_FIELDS( FieldT, temperature_, cp_, cv_, mixMolWeight_ )
  DECLARE_FIELDS( FieldT, l1_, l2_, l345D_, l345O1_, l345O2_ )
  DECLARE_VECTOR_OF_FIELDS( FieldT, enthalpies_ )
  DECLARE_VECTOR_OF_FIELDS( FieldT, lpks_ )

  const bool doPerp1_, doPerp2_;
  const bool doSpecies_;
  const NSCBC::NSCBCInfo<FieldT> info_;
  const SpatialOps::SpatialMask<FieldT> mask_;
  const double side_;
  const std::vector<double>& specMW_;
  const size_t nspec_;
  const double gasConstant_;
  const double addsub_;
  double curl_;
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################
template< typename FieldT >
EnergyNSCBCModifier<FieldT>::
EnergyNSCBCModifier( const NSCBC::NSCBCInfo<FieldT>& info,
                     const double side,
                     const Expr::Tag& eTag,
                     const Expr::Tag& cTag,
                     const Expr::Tag& densityTag,
                     const Expr::Tag& velDTag,
                     const Expr::Tag& velO1Tag,
                     const Expr::Tag& velO2Tag,
                     const Expr::Tag& temperatureTag,
                     const Expr::Tag& cpTag,
                     const Expr::Tag& cvTag,
                     const Expr::Tag& mixMolWeightTag,
                     const Expr::TagList& enthalpyTags,
                     const Expr::Tag& l1Tag,
                     const Expr::Tag& l2Tag,
                     const Expr::Tag& l345DTag,
                     const Expr::Tag& l345O1Tag,
                     const Expr::Tag& l345O2Tag,
                     const Expr::TagList& lpkTags,
                     const std::vector<double>& specMW,
                     const double gasConstant,
                     const bool doSpecies,
                     const double addsub )
  : Expr::Expression<FieldT>(),
    doPerp1_    ( velO1Tag.context() != Expr::INVALID_CONTEXT ),
    doPerp2_    ( velO2Tag.context() != Expr::INVALID_CONTEXT ),
    doSpecies_  ( doSpecies     ),
    info_       ( info          ),
    mask_       ( info_.mask    ),
    side_       ( side          ),
    specMW_     ( specMW        ),
    nspec_      ( specMW.size() ),
    gasConstant_( gasConstant   ),
    addsub_     ( addsub        )
{
  this->set_gpu_runnable( true );

  //  assert( enthalpyTags.size() == nspec_ );

  if( doPerp1_ ){
    velO1_  = this->template create_field_request<FieldT>( velO1Tag  );
    l345O2_ = this->template create_field_request<FieldT>( l345O2Tag );
  }
  if( doPerp2_ ){
    velO2_  = this->template create_field_request<FieldT>( velO2Tag  );
    l345O1_ = this->template create_field_request<FieldT>( l345O1Tag );
  }

  e_            = this->template create_field_request<FieldT>( eTag            );
  cp_           = this->template create_field_request<FieldT>( cpTag           );
  cv_           = this->template create_field_request<FieldT>( cvTag           );
  c_            = this->template create_field_request<FieldT>( cTag            );
  density_      = this->template create_field_request<FieldT>( densityTag      );
  velD_         = this->template create_field_request<FieldT>( velDTag         );
  temperature_  = this->template create_field_request<FieldT>( temperatureTag  );
  mixMolWeight_ = this->template create_field_request<FieldT>( mixMolWeightTag );
  l1_           = this->template create_field_request<FieldT>( l1Tag           );
  l2_           = this->template create_field_request<FieldT>( l2Tag           );
  l345D_        = this->template create_field_request<FieldT>( l345DTag        );

  if( doSpecies_ ){
    this->template create_field_vector_request<FieldT>( enthalpyTags, enthalpies_ );
    this->template create_field_vector_request<FieldT>( lpkTags,  lpks_   );
  }

  //Curl is the right hand rule from the d direction onto the boundary
  //d2 on a  Y-boundary has a positive curl, Z-boundary has a negative curl
  //d3 on an X-boundary has a negative curl, Z-boundary has a positive curl
  //d4 on an X-boundary has a positive curl, Z-boundary has a negative curl
  //This is multiplied by an additional term in the evaluate expression, however,
  //based on which d we choose.  Suffice to say, Y boundaries need a -1.0 multiplier
  //because we floor the perpendicular directions (e.g. perp1 is d2 for the y direction
  //but d3 for the x direction).
  switch( info_.direction ){
    case SpatialOps::XDIR ::value: curl_ =  1.0; break;
    case SpatialOps::YDIR ::value: curl_ = -1.0; break;
    case SpatialOps::ZDIR ::value: curl_ =  1.0; break;
    case SpatialOps::NODIR::value:
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "No direction in the Energy modifier" << std::endl;
      throw std::runtime_error( msg.str() );
  }
}

//--------------------------------------------------------------------

template< typename FieldT >
void
EnergyNSCBCModifier<FieldT>::
evaluate()
{
  using namespace SpatialOps;

  FieldT& result = this->value();

  const FieldT& e             = e_           ->field_ref();
  const FieldT& cp            = cp_          ->field_ref();
  const FieldT& cv            = cv_          ->field_ref();
  const FieldT& c             = c_           ->field_ref();
  const FieldT& density       = density_     ->field_ref();
  const FieldT& velD          = velD_        ->field_ref();
  const FieldT& temperature   = temperature_ ->field_ref();
  const FieldT& mixMolWeight  = mixMolWeight_->field_ref();
  const FieldT& l1            = l1_          ->field_ref();
  const FieldT& l2            = l2_          ->field_ref();
  const FieldT& l345D         = l345D_       ->field_ref();

  SpatFldPtr<FieldT> Rmix        = SpatialFieldStore::get<FieldT>( result );
  SpatFldPtr<FieldT> XiN         = SpatialFieldStore::get<FieldT>( result );
  SpatFldPtr<FieldT> d1          = SpatialFieldStore::get<FieldT>( result );
  SpatFldPtr<FieldT> d234D       = SpatialFieldStore::get<FieldT>( result );
  SpatFldPtr<FieldT> d5          = SpatialFieldStore::get<FieldT>( result );
  SpatFldPtr<FieldT> dpn         = SpatialFieldStore::get<FieldT>( result );
  SpatFldPtr<FieldT> momentumSum = SpatialFieldStore::get<FieldT>( result );
  SpatFldPtr<FieldT> speciesSum  = SpatialFieldStore::get<FieldT>( result );
  SpatFldPtr<FieldT> coefficient = SpatialFieldStore::get<FieldT>( result );

  masked_assign( mask_, *Rmix, gasConstant_ / mixMolWeight );

  nscbc_d1         ( *d1,     mask_, side_, l1, l2, l345D, c );
  nscbc_d234diag   ( *d234D,  mask_, side_, l1, l2, c, density );
  nscbc_d5         ( *d5,     mask_, l1, l2  );

  *speciesSum <<= 0.0;
  if( doSpecies_ ){
    const FieldT& nenthalpy = enthalpies_[nspec_-1]->field_ref();
    masked_assign( mask_, *XiN, nenthalpy - cp * temperature * mixMolWeight / specMW_[nspec_-1] );

    for( size_t i=0; i<nspec_-1; ++i ){
      const FieldT& enthalpy = enthalpies_[i]->field_ref();
      const FieldT& lpk = lpks_[i]->field_ref();
      masked_assign( mask_, *coefficient, enthalpy - cp * temperature * mixMolWeight / specMW_[i] - *XiN );
      nscbc_d5pn( *dpn, mask_, lpk );
      *speciesSum <<=  *speciesSum + *coefficient * *dpn;
    }
  }
  else{
    *speciesSum <<= 0.0;
  }

  *momentumSum <<= velD * *d234D;
  if( doPerp1_ ){
    const FieldT& velO1       = velO1_ ->field_ref();
    const FieldT& l345O2      = l345O2_->field_ref();
    SpatFldPtr<FieldT> d234O1 = SpatialFieldStore::get<FieldT>( result );

    nscbc_d234offdiag( *d234O1, mask_, side_, curl_ * ( 1.0), l345O2 );

    *momentumSum <<= *momentumSum + velO1 * *d234O1;
  }
  if( doPerp2_ ){
    const FieldT& velO2       = velO2_ ->field_ref();
    const FieldT& l345O1      = l345O1_->field_ref();
    SpatFldPtr<FieldT> d234O2 = SpatialFieldStore::get<FieldT>( result );

    nscbc_d234offdiag( *d234O2, mask_, side_, curl_ * (-1.0), l345O1 );

    *momentumSum <<= *momentumSum + velO2 * *d234O2;
  }

  masked_assign( mask_, result, result - (addsub_) * (   ( e - cv * temperature ) * *d1
                                                       + ( density              ) * *momentumSum
                                                       + ( cv / (*Rmix)         ) * *d5
                                                       + ( density              ) * *speciesSum  ) );
}

//--------------------------------------------------------------------
// 3D case
template< typename FieldT >
EnergyNSCBCModifier<FieldT>::Builder::
Builder( const Expr::Tag& result,
         const NSCBC::NSCBCInfo<FieldT>& info,
         const double side,
         const Expr::Tag& eTag,
         const Expr::Tag& cTag,
         const Expr::Tag& densityTag,
         const Expr::Tag& velDTag,
         const Expr::Tag& velO1Tag,
         const Expr::Tag& velO2Tag,
         const Expr::Tag& temperatureTag,
         const Expr::Tag& cpTag,
         const Expr::Tag& cvTag,
         const Expr::Tag& mixMolWeightTag,
         const Expr::TagList& enthalpyTags,
         const Expr::Tag& l1Tag,
         const Expr::Tag& l2Tag,
         const Expr::Tag& l345DTag,
         const Expr::Tag& l345O1Tag,
         const Expr::Tag& l345O2Tag,
         const Expr::TagList& lpkTags,
         const std::vector<double>& specMW,
         const double gasConstant,
         const bool doSpecies,
         const double addsub )
  : ExpressionBuilder(result),
    info_         ( info            ),
    side_         ( side            ),
    eT_           ( eTag            ),
    cT_           ( cTag            ),
    densityT_     ( densityTag      ),
    velDT_        ( velDTag         ),
    velO1T_       ( velO1Tag        ),
    velO2T_       ( velO2Tag        ),
    temperatureT_ ( temperatureTag  ),
    cpT_          ( cpTag           ),
    cvT_          ( cvTag           ),
    mixMolWeightT_( mixMolWeightTag ),
    enthalpyT_    ( enthalpyTags    ),
    l1T_          ( l1Tag           ),
    l2T_          ( l2Tag           ),
    l345DT_       ( l345DTag        ),
    l345O1T_      ( l345O1Tag       ),
    l345O2T_      ( l345O2Tag       ),
    lpkT_         ( lpkTags         ),
    specMW_       ( specMW          ),
    gasConstant_  ( gasConstant     ),
    doSpecies_    ( doSpecies       ),
    addsub_       ( addsub          )
{}

//--------------------------------------------------------------------
//2D case
template< typename FieldT >
EnergyNSCBCModifier<FieldT>::Builder::
Builder( const Expr::Tag& result,
         const NSCBC::NSCBCInfo<FieldT>& info,
         const double side,
         const Expr::Tag& eTag,
         const Expr::Tag& cTag,
         const Expr::Tag& densityTag,
         const Expr::Tag& velDTag,
         const Expr::Tag& velO1Tag,
         const Expr::Tag& temperatureTag,
         const Expr::Tag& cpTag,
         const Expr::Tag& cvTag,
         const Expr::Tag& mixMolWeightTag,
         const Expr::TagList& enthalpyTags,
         const Expr::Tag& l1Tag,
         const Expr::Tag& l2Tag,
         const Expr::Tag& l345DTag,
         const Expr::Tag& l345O2Tag,
         const Expr::TagList& lpkTags,
         const std::vector<double>& specMW,
         const double gasConstant,
         const bool doSpecies,
         const double addsub )
  : ExpressionBuilder(result),
    info_         ( info                      ),
    side_         ( side                      ),
    eT_           ( eTag                      ),
    cT_           ( cTag                      ),
    densityT_     ( densityTag                ),
    velDT_        ( velDTag                   ),
    velO1T_       ( velO1Tag                  ),
    velO2T_       ( "", Expr::INVALID_CONTEXT ),
    temperatureT_ ( temperatureTag            ),
    cpT_          ( cpTag                     ),
    cvT_          ( cvTag                     ),
    mixMolWeightT_( mixMolWeightTag           ),
    enthalpyT_    ( enthalpyTags              ),
    l1T_          ( l1Tag                     ),
    l2T_          ( l2Tag                     ),
    l345DT_       ( l345DTag                  ),
    l345O1T_      ( "", Expr::INVALID_CONTEXT ),
    l345O2T_      ( l345O2Tag                 ),
    lpkT_         ( lpkTags                   ),
    specMW_       ( specMW                    ),
    gasConstant_  ( gasConstant               ),
    doSpecies_    ( doSpecies                 ),
    addsub_       ( addsub                    )
{}

//--------------------------------------------------------------------

template< typename FieldT >
EnergyNSCBCModifier<FieldT>::Builder::
Builder( const Expr::Tag& result,
         const NSCBC::NSCBCInfo<FieldT>& info,
         const double side,
         const Expr::Tag& eTag,
         const Expr::Tag& cTag,
         const Expr::Tag& densityTag,
         const Expr::Tag& velDTag,
         const Expr::Tag& temperatureTag,
         const Expr::Tag& cpTag,
         const Expr::Tag& cvTag,
         const Expr::Tag& mixMolWeightTag,
         const Expr::TagList& enthalpyTags,
         const Expr::Tag& l1Tag,
         const Expr::Tag& l2Tag,
         const Expr::Tag& l345DTag,
         const Expr::TagList& lpkTags,
         const std::vector<double>& specMW,
         const double gasConstant,
         const bool doSpecies,
         const double addsub )
  : ExpressionBuilder(result),
    info_         ( info                      ),
    side_         ( side                      ),
    eT_           ( eTag                      ),
    cT_           ( cTag                      ),
    densityT_     ( densityTag                ),
    velDT_        ( velDTag                   ),
    velO1T_       ( "", Expr::INVALID_CONTEXT ),
    velO2T_       ( "", Expr::INVALID_CONTEXT ),
    temperatureT_ ( temperatureTag            ),
    cpT_          ( cpTag                     ),
    cvT_          ( cvTag                     ),
    mixMolWeightT_( mixMolWeightTag           ),
    enthalpyT_    ( enthalpyTags              ),
    l1T_          ( l1Tag                     ),
    l2T_          ( l2Tag                     ),
    l345DT_       ( l345DTag                  ),
    l345O1T_      ( "", Expr::INVALID_CONTEXT ),
    l345O2T_      ( "", Expr::INVALID_CONTEXT ),
    lpkT_         ( lpkTags                   ),
    specMW_       ( specMW                    ),
    gasConstant_  ( gasConstant               ),
    doSpecies_    ( doSpecies                 ),
    addsub_       ( addsub                    )
{}


//--------------------------------------------------------------------

template< typename FieldT >
Expr::ExpressionBase*
EnergyNSCBCModifier<FieldT>::Builder::build() const
{
  return new EnergyNSCBCModifier<FieldT>(info_,side_,eT_,cT_,densityT_,velDT_,velO1T_,velO2T_,temperatureT_,cpT_,cvT_,
                                         mixMolWeightT_,enthalpyT_,l1T_,l2T_,l345DT_,l345O1T_,l345O2T_,lpkT_,
                                         specMW_, gasConstant_, doSpecies_, addsub_ );
}

//--------------------------------------------------------------------

#endif /* EnergyNSCBCModifier_h */
