/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *      \file   MomentumOffDiagonalModifier.h
 *      \date   Created on: May 20, 2015
 *      \author Author: Derek Cline
 */

#ifndef MomentumOffDiagonalModifier_h
#define MomentumOffDiagonalModifier_h

#include <expression/ExprLib.h>
#include "D.h"

/**
 * \class MomentumOffDiagonalModifier
 * \brief Modifier expression required to update the boundaries
 *        of the density equation
 */
template< typename FieldT >
class MomentumOffDiagonalModifier : public Expr::Expression<FieldT>
{
public:

  class Builder : public Expr::ExpressionBuilder
  {
    const SpatialOps::SpatialMask<FieldT> mask_;
    const double side_;
    const double curl_;
    const Expr::Tag densityT_, cT_, velocityT_;
    const Expr::Tag l1T_, l2T_, l345DT_, l345OT_;
    const double addsub_;
  public:
    Builder( const Expr::Tag& result,
             const SpatialOps::SpatialMask<FieldT>& mask,
             const double side,
             const double curl,
             const Expr::Tag& densityTag,
             const Expr::Tag& cTag,
             const Expr::Tag& velocityTag,
             const Expr::Tag& l1Tag,
             const Expr::Tag& l2Tag,
             const Expr::Tag& l345DTag,
             const Expr::Tag& l345OTag,
             const double addsub )
      : ExpressionBuilder(result),
        mask_      ( mask         ),
        side_      ( side         ),
        curl_      ( curl         ),
        densityT_  ( densityTag   ),
        cT_        ( cTag         ),
        velocityT_ ( velocityTag  ),
        l1T_       ( l1Tag        ),
        l2T_       ( l2Tag        ),
        l345DT_    ( l345DTag     ),
        l345OT_    ( l345OTag     ),
        addsub_    ( addsub       )
    {}

    Expr::ExpressionBase* build() const{
      return new MomentumOffDiagonalModifier<FieldT>(mask_,side_,curl_,densityT_,cT_,velocityT_,l1T_,l2T_,l345DT_,l345OT_,addsub_);
    }
  };

  void evaluate();

private:

  MomentumOffDiagonalModifier( const SpatialOps::SpatialMask<FieldT>& mask,
                               const double side,
                               const double curl,
                               const Expr::Tag& densityTag,
                               const Expr::Tag& cTag,
                               const Expr::Tag& velocityTag,
                               const Expr::Tag& l1Tag,
                               const Expr::Tag& l2Tag,
                               const Expr::Tag& l345DTag,
                               const Expr::Tag& l345OTag,
                               const double addsub );

  ~MomentumOffDiagonalModifier(){}

  DECLARE_FIELDS( FieldT, density_, vel_, c_, velocity_ )
  DECLARE_FIELDS( FieldT, l1_, l2_, l345D_, l345O_ )

  const SpatialOps::SpatialMask<FieldT> mask_;
  const double side_;
  const double curl_;
  const double addsub_;
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################
template< typename FieldT >
MomentumOffDiagonalModifier<FieldT>::
MomentumOffDiagonalModifier( const SpatialOps::SpatialMask<FieldT>& mask,
                       const double side,
                       const double curl,
                       const Expr::Tag& densityTag,
                       const Expr::Tag& cTag,
                       const Expr::Tag& velocityTag,
                       const Expr::Tag& l1Tag,
                       const Expr::Tag& l2Tag,
                       const Expr::Tag& l345DTag,
                       const Expr::Tag& l345OTag,
                       const double addsub )
  : Expr::Expression<FieldT>(),
    mask_  ( mask   ),
    side_  ( side   ),
    curl_  ( curl   ),
    addsub_( addsub )
{
  this->set_gpu_runnable( true );

  c_        = this->template create_field_request<FieldT>( cTag        );
  density_  = this->template create_field_request<FieldT>( densityTag  );
  velocity_ = this->template create_field_request<FieldT>( velocityTag );
  l1_       = this->template create_field_request<FieldT>( l1Tag       );
  l2_       = this->template create_field_request<FieldT>( l2Tag       );
  l345D_    = this->template create_field_request<FieldT>( l345DTag    );
  l345O_    = this->template create_field_request<FieldT>( l345OTag    );
}

//--------------------------------------------------------------------

template< typename FieldT >
void
MomentumOffDiagonalModifier<FieldT>::
evaluate()
{
  using namespace SpatialOps;

  FieldT& result = this->value();

  const FieldT& c         = c_        ->field_ref();
  const FieldT& density   = density_  ->field_ref();
  const FieldT& velocity  = velocity_ ->field_ref();
  const FieldT& l1        = l1_       ->field_ref();
  const FieldT& l2        = l2_       ->field_ref();
  const FieldT& l345D     = l345D_    ->field_ref();
  const FieldT& l345O     = l345O_    ->field_ref();

  SpatFldPtr<FieldT> d1    = SpatialFieldStore::get<FieldT>( result );
  SpatFldPtr<FieldT> d234  = SpatialFieldStore::get<FieldT>( result );

  nscbc_d1(          *d1,   mask_, side_, l1,  l2,  l345D,  c );
  nscbc_d234offdiag( *d234, mask_, side_, curl_, l345O  );

  masked_assign( mask_, result, result - (addsub_)*( velocity * *d1 + density * *d234 ) );
}

//--------------------------------------------------------------------

#endif /* MomentumOffDiagonalModifier_h */
