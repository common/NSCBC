/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *      \file   SpeciesNSCBCModifier.h
 *      \date   Created on: Mar 20, 2015
 *      \author Author: Derek Cline
 *
 *      \class SpeciesNSCBCModifier
 *      \brief SpeciesNSCBCModifier an expression for Navier-Stokes Characteristic
 *             Boundary Condition Calculations in the ODT code database.  See
 *             Sutherland and Kennedy 2003 or Coussement et al. 2012 for formulations
 *
 *             This modifier will be attached as a modifier expression of the RHS
 *             of each Species Transport Equation in the Navier-Stokes Eqns
 *             twice for each species term, first to subtract the calculated wave
 *             amplitudes and then to add back in the set wave amplitudes
 *
 *      \param mask Spatial Mask associated with the boundary cells
 *      \param side SpatialOps side (plus or minus)
 *      \param densityTag Density
 *      \param cTag Speed of Sound
 *      \param speciesTag Species fraction to be transported
 *      \param l1Tag First wave amplitude (set for addition, calculated for subtraction)
 *      \param l2Tag Second wave amplitude (set for addition, calculated for subtraction)
 *      \param l345Tag Diagonal wave amplitude (L3 = X, L4 = Y, L5 = Z boundary).
 *      \param lpnTag Lk term associated with this species term
 *      \tparam FieldT Volume Field Type
 */

#ifndef SpeciesNSCBCModifier_h
#define SpeciesNSCBCModifier_h

#include <expression/ExprLib.h>
#include "D.h"

template< typename FieldT >
class SpeciesNSCBCModifier : public Expr::Expression<FieldT>
{
public:

  class Builder : public Expr::ExpressionBuilder
  {
    const SpatialOps::SpatialMask<FieldT> mask_;
    const double side_;
    const Expr::Tag densityT_, cT_, speciesT_;
    const Expr::Tag l1T_, l2T_, l345T_, lpnT_;
    const double addsub_;
  public:
    Builder( const Expr::Tag& result,
             const SpatialOps::SpatialMask<FieldT>& mask,
             const double side,
             const Expr::Tag& densityTag,
             const Expr::Tag& cTag,
             const Expr::Tag& speciesTag,
             const Expr::Tag& l1Tag,
             const Expr::Tag& l2Tag,
             const Expr::Tag& l345Tag,
             const Expr::Tag& lpnTag,
             const double addsub )
      : ExpressionBuilder(result),
        mask_      ( mask         ),
        side_      ( side         ),
        densityT_  ( densityTag   ),
        cT_        ( cTag         ),
        speciesT_  ( speciesTag   ),
        l1T_       ( l1Tag        ),
        l2T_       ( l2Tag        ),
        l345T_     ( l345Tag      ),
        lpnT_      ( lpnTag       ),
        addsub_    ( addsub       )
    {}

    Expr::ExpressionBase* build() const{
      return new SpeciesNSCBCModifier<FieldT>(mask_,side_,densityT_,cT_,speciesT_,l1T_,l2T_,l345T_,lpnT_, addsub_);
    }
  };

  void evaluate();

private:

  SpeciesNSCBCModifier( const SpatialOps::SpatialMask<FieldT>& mask,
                        const double side,
                        const Expr::Tag& densityTag,
                        const Expr::Tag& cTag,
                        const Expr::Tag& speciesTag,
                        const Expr::Tag& l1Tag,
                        const Expr::Tag& l2Tag,
                        const Expr::Tag& l345Tag,
                        const Expr::Tag& lpnTag,
                        const double addsub );

  ~SpeciesNSCBCModifier(){}

  DECLARE_FIELDS( FieldT, density_, c_, species_ )
  DECLARE_FIELDS( FieldT, l1_, l2_, l345_, lpn_ )

  const SpatialOps::SpatialMask<FieldT> mask_;
  const double side_;
  const double addsub_;
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################
template< typename FieldT >
SpeciesNSCBCModifier<FieldT>::
SpeciesNSCBCModifier( const SpatialOps::SpatialMask<FieldT>& mask,
                      const double side,
                      const Expr::Tag& densityTag,
                      const Expr::Tag& cTag,
                      const Expr::Tag& speciesTag,
                      const Expr::Tag& l1Tag,
                      const Expr::Tag& l2Tag,
                      const Expr::Tag& l345Tag,
                      const Expr::Tag& lpnTag,
                      const double addsub )
  : Expr::Expression<FieldT>(),
    mask_  ( mask   ),
    side_  ( side   ),
    addsub_( addsub )
{
  this->set_gpu_runnable( true );

  c_       = this->template create_field_request<FieldT>( cTag       );
  density_ = this->template create_field_request<FieldT>( densityTag );
  species_ = this->template create_field_request<FieldT>( speciesTag );
  l1_      = this->template create_field_request<FieldT>( l1Tag      );
  l2_      = this->template create_field_request<FieldT>( l2Tag      );
  l345_    = this->template create_field_request<FieldT>( l345Tag    );
  lpn_     = this->template create_field_request<FieldT>( lpnTag     );
}

//--------------------------------------------------------------------

template< typename FieldT >
void
SpeciesNSCBCModifier<FieldT>::
evaluate()
{
  using namespace SpatialOps;

  FieldT& result = this->value();

  const FieldT& c       = c_      ->field_ref();
  const FieldT& density = density_->field_ref();
  const FieldT& species = species_->field_ref();
  const FieldT& l1      = l1_     ->field_ref();
  const FieldT& l2      = l2_     ->field_ref();
  const FieldT& l345    = l345_   ->field_ref();
  const FieldT& lpn     = lpn_    ->field_ref();

  SpatFldPtr<FieldT> d1  = SpatialFieldStore::get<FieldT>( result );
  SpatFldPtr<FieldT> dpn = SpatialFieldStore::get<FieldT>( result );

  nscbc_d1(   *d1,  mask_, side_, l1,  l2,  l345, c );
  nscbc_d5pn( *dpn, mask_, lpn );

  masked_assign( mask_, result, result - (addsub_)*( species * *d1 + density * *dpn ));
}

//--------------------------------------------------------------------

#endif /* SpeciesNSCBCModifier_h */
