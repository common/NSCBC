/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/*
 * headers_test.cpp
 *
 *  Created on: Mar 25, 2015
 *      Author: derek
 *      \brief for testing (i.e. compilation debugging) purposes
 */

#include <nscbc/NSCBCToolsAndDefs.h>
#include <nscbc/CharacteristicBCBuilder.h>
#include <nscbc/SpeedOfSound.h>
#include <nscbc/TagManager.h>

#include <nscbc/Lexpressions/L1L2.h>
#include <nscbc/Lexpressions/L1Set.h>
#include <nscbc/Lexpressions/L345Set.h>
#include <nscbc/Lexpressions/L3L4L5Diagonal.h>
#include <nscbc/Lexpressions/L3L4L5OffDiagonal.h>
#include <nscbc/Lexpressions/Lk.h>
#include <nscbc/Lexpressions/LkSet.h>
#include <nscbc/Lexpressions/RPressure.h>

#include <nscbc/modifierexpressions/DensityNSCBCModifier.h>
#include <nscbc/modifierexpressions/EnergyNSCBCModifier.h>
#include <nscbc/modifierexpressions/MomentumNSCBCModifier.h>
#include <nscbc/modifierexpressions/MomentumOffDiagonalModifier.h>
#include <nscbc/modifierexpressions/SpeciesNSCBCModifier.h>

int main(){
  std::cout << "This test has all relevant #include directives except CharacteristicBCBuilder.h\n"
            << "No operations are performed and it returns pass, analogous to an exam in Writing 101\n";
  return 0;
}




