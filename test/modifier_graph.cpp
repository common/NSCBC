/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/*
 * build_modifiers.cpp
 *
 *  Created on: Mar 29, 2015
 *      Author: nate
 */

#include <nscbc/CharacteristicBCBuilder.h>
#include <nscbc/TagManager.h>

#include <expression/ExprLib.h>

#include <spatialops/structured/FVStaggeredFieldTypes.h>
#include <spatialops/structured/Grid.h>

#include "simple_transport.h"

namespace SO = SpatialOps;
typedef SO::SVolField  CellField;
using SO::IntVec;
using Expr::Tag;
using Expr::STATE_NONE;
using std::cout;
using std::endl;


int main()
{
  std::map< NSCBC::TagName,     Expr::Tag > tags;
  tags[NSCBC::U  ] = Tag( "xVel", STATE_NONE );
  tags[NSCBC::V  ] = Tag( "yVel", STATE_NONE );
  tags[NSCBC::W  ] = Tag( "zVel", STATE_NONE );
  tags[NSCBC::T  ] = Tag( "T", STATE_NONE );
  tags[NSCBC::P  ] = Tag( "P", STATE_NONE );
  tags[NSCBC::RHO] = Tag( "Rho", STATE_NONE );
  tags[NSCBC::MMW] = Tag( "MixMW", STATE_NONE );
  tags[NSCBC::CP ] = Tag( "Cp", STATE_NONE );
  tags[NSCBC::CV ] = Tag( "Cv", STATE_NONE );
  tags[NSCBC::E0 ] = Tag( "E0", STATE_NONE );
  NSCBC::TagManager tMgr( tags, false );

  IntVec gridSize( 6,  6,  1 );
  Expr::ExprPatch patch( gridSize[0], gridSize[1], gridSize[2] );
  const SO::Grid grid( gridSize, SO::DoubleVec( 1, 1, 1 ) );
  const SO::GhostData ghosts( IntVec( 1, 1, 0), IntVec( 1, 1, 0 ) ); // 1 on +-x and +- y and 0 on z
  const SO::IntVec hasBC(true,true,true);
  const SO::BoundaryCellInfo bcInfo = SO::BoundaryCellInfo::build<CellField>( hasBC, hasBC );
  const SO::MemoryWindow window( SpatialOps::get_window_with_ghost( grid.extent(), ghosts, bcInfo ) );
  //build mask
  std::vector<IntVec> xminusPts, xplusPts, yminusPts, yplusPts;

  for( int i=0; i < grid.extent(1); ++i ){
    xminusPts.push_back( SpatialOps::IntVec(0,              i, 0) );
    xplusPts. push_back( SpatialOps::IntVec(grid.extent(0), i, 0) );
    std::cout << SpatialOps::IntVec(0,i,0) << " ... " << SpatialOps::IntVec(grid.extent(0),i,0) << std::endl;
  }
  for(int i = 0; i < grid.extent(0); ++i){
    yminusPts.push_back( SpatialOps::IntVec(i,              0, 0) );
    yplusPts. push_back( SpatialOps::IntVec(i, grid.extent(1), 0) );
    std::cout << SpatialOps::IntVec(i,0,0) << " ... " << SpatialOps::IntVec(i,grid.extent(1), 0) << std::endl;
  }
  std::cout << window << std::endl << bcInfo << std::endl << ghosts << std::endl;
  const SO::SpatialMask<CellField> xMinusMask( window, bcInfo, ghosts, xminusPts );
  const SO::SpatialMask<CellField> xPlusMask ( window, bcInfo, ghosts, xplusPts  );
  const SO::SpatialMask<CellField> yMinusMask( window, bcInfo, ghosts, yminusPts );
  const SO::SpatialMask<CellField> yPlusMask ( window, bcInfo, ghosts, yplusPts  );

  NSCBC::NSCBCInfo<CellField> xMinusBC( xMinusMask, SO::MINUS_SIDE, NSCBC::XDIR, NSCBC::NONREFLECTING);
  NSCBC::NSCBCInfo<CellField> xPlusBC ( xPlusMask,  SO::PLUS_SIDE,  NSCBC::XDIR, NSCBC::NONREFLECTING);
  NSCBC::NSCBCInfo<CellField> yMinusBC( yMinusMask, SO::MINUS_SIDE, NSCBC::YDIR, NSCBC::NONREFLECTING);
  NSCBC::NSCBCInfo<CellField> yPlusBC ( yPlusMask,  SO::PLUS_SIDE,  NSCBC::YDIR, NSCBC::NONREFLECTING);

  std::vector<double> specMW; // empty for no species
  const double gasConstant = 0.0;  // no species
  NSCBC::BCBuilder<CellField> xMBuilder( xMinusBC, specMW, gasConstant, tMgr, true, true );
  NSCBC::BCBuilder<CellField> xPBuilder( xPlusBC,  specMW, gasConstant, tMgr, true, true );
  NSCBC::BCBuilder<CellField> yMBuilder( yMinusBC, specMW, gasConstant, tMgr, true, true );
  NSCBC::BCBuilder<CellField> yPBuilder( yPlusBC,  specMW, gasConstant, tMgr, true, true );

  Expr::ExpressionFactory execFactory;
  typedef Expr::PlaceHolder<CellField>::Builder PlaceHolder;
  execFactory.register_expression( new PlaceHolder( tMgr[NSCBC::U] ) );
  execFactory.register_expression( new PlaceHolder( tMgr[NSCBC::V] ) );
  execFactory.register_expression( new PlaceHolder( tMgr[NSCBC::W] ) );
  execFactory.register_expression( new PlaceHolder( tMgr[NSCBC::T] ) );
  execFactory.register_expression( new PlaceHolder( tMgr[NSCBC::P] ) );
  execFactory.register_expression( new PlaceHolder( tMgr[NSCBC::RHO] ) );
  execFactory.register_expression( new PlaceHolder( tMgr[NSCBC::MMW] ) );
  execFactory.register_expression( new PlaceHolder( tMgr[NSCBC::CP] ) );
  execFactory.register_expression( new PlaceHolder( tMgr[NSCBC::CV] ) );
  execFactory.register_expression( new PlaceHolder( tMgr[NSCBC::E0] ) );

  typedef NSCBC::SimpleTransport<CellField> SimpleTransport;
  SimpleTransport* simple = new SimpleTransport( execFactory );
//
//  simple->setup_boundary_conditions( execFactory, xMBuilder );
//  simple->setup_boundary_conditions( execFactory, xPBuilder );
//  simple->setup_boundary_conditions( execFactory, yMBuilder );
//  simple->setup_boundary_conditions( execFactory, yPBuilder );

//  Expr::TimeStepper timeIntegrator( execFactory, Expr::FORWARD_EULER, "timestepper", patch.id() );
//
//  timeIntegrator.add_equation<CellField>( simple->solution_variable_name(), simple->get_rhs_tag(), ghosts );
//
//  Expr::FieldManagerList& fml = patch.field_manager_list();
//
//  fml.allocate_fields( patch.field_info() );
//
//  SO::OperatorDatabase& opDB = patch.operator_database();
//
//  SO::build_stencils( grid, opDB );
//
//  timeIntegrator.finalize( fml, opDB, patch.field_info() );
//
//  {
//    std::ofstream out( "simple_tree.dot" );
//    timeIntegrator.get_tree()->write_tree( out );
//  }

  return 0;

}
