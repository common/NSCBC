/*
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/*
 * simple_transport.h
 *
 *  Created on: Mar 29, 2015
 *      Author: nate
 */

#ifndef SIMPLE_TRANSPORT_H_
#define SIMPLE_TRANSPORT_H_

namespace NSCBC{

template< typename FieldT >
class SimpleTransport : public Expr::TransportEquation
{

public:

  SimpleTransport( Expr::ExpressionFactory& execFactory );

  ~SimpleTransport() {}

  void setup_boundary_conditions( Expr::ExpressionFactory& execFactory, BCBuilder<FieldT>& bcBuilder );

private:

  // overwrite base class virtual functions
  void setup_boundary_conditions( Expr::ExpressionFactory& execFactory ){}

};

//====================================================================

template< typename FieldT >
SimpleTransport<FieldT>::
SimpleTransport( Expr::ExpressionFactory& execFactory )
 : Expr::TransportEquation( "trans",
   Expr::Tag( "trans_RHS", Expr::STATE_NONE ) )
{

  typedef typename Expr::ConstantExpr<FieldT>::Builder RHS;
  execFactory.register_expression( new RHS ( this->get_rhs_tag(), 1.0 ) );

}

template< typename FieldT >
void
SimpleTransport<FieldT>::
setup_boundary_conditions( Expr::ExpressionFactory& execFactory, BCBuilder<FieldT>& bcBuilder )
{

  bcBuilder.attach_rhs_modifier( execFactory, this->get_rhs_tag(), NSCBC::DENSITY, -65768 );

}

} // namespace NSCBC
#endif /* SIMPLE_TRANSPORT_H_ */
